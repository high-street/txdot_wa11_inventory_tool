# Active Transportation Plan Inventory (ATPI)

The Active Transportation Plan Inventory is a map-based inventory of bicycle and pedestrian-related transportation plans published by planning entities at varying geographic levels across the state. It was created by High Street for TxDOT to assist planning entities, designers, engineers, and other planning professionals focused on active transportation with collaboration and coordination during project development by serving as a central repository of existing active transportation planning documents. 

For more details, see: https://www.txdot.gov/inside-txdot/modes-of-travel/bicycle/plan-inventory-tool.html


## Project Structure

The ATPI Consists of three applications:

1. Plan Submission Portal (`Deployment/PlanSubmissionPortal`) - Used by TxDOT planning entities to submit plan information
2. Plan Management Portal (`Deployment/PlanManagementPortal`) - Used by TxDOT PTN staff to manage plan information submitted through the Plan Submission Portal
3. Plan Viewer (`Deployment/Plans`) - Used by members of the general public to access the inventory of Active Transportation Plans

## Project Team

- TxDOT
	- Noah Heath - Project Manager - Noah.Heath@txdot.gov
	- Bonnie Sherman - Bonnie.Sherman@txdot.gov
	- Carl Seifert - CSEIFE-C@txdot.gov

- High Street
	- Chapman Munn - Software Developer - munn@highstreetconsulting.com
	- Rebecca Van Dyke - Project Manager - vandyke@highstreetconsulting.com
	- Mark Egge - Implementation Manager - egge@highstreetconsulting.com
	- Mackenzie Bartek - UI/UX - bartek@highstreetconsulting.com

## Knowledge Transfer
TxDOT and High Street had a recorded high-level knowledge transfer session in 2022. That recording (KnowledgeTransfer/KnowledgeTransfer.mp4) is available in the KnowledgeTransfer folder contained within the project repository.

## Project Repository

The ATPI applications and all supporting information are contained within the project Bitbucket repository:

https://bitbucket.org/high-street/txdot_wa11_inventory_tool/src/master/


# Deployment

The ATPI applications rely on ArcGIS Online hosted services (API) and static HTML5 files delivered to the end user's web browser via an HTTP web server. Deploying the ATPI applications has two steps:

1. Configure the required ArcGIS Online Resources (hosted feature layers and web maps)
2. Host the static HTML5 files on a web server at the appropriate URL (and with appropriate SSL certificates)

## Configuring Required ArcGIS Online Resources

The ATPI applications require a series of user-configured Esri ArcGIS Online resources. Below is a list of required items and pertinent information about those resources.

### ATPI Feature Services

The inventory is stored in a series of feature layers hosted in ArcGIS Online. These are described below.

**Inventory Updates** - this hosted feature layer is used to house temporary plan information. Plans are removed from the layer when PTN either validates or rejects a plan. Members of the public are considered "anonymous editors" and may add features but may not view, update, or delete features (except for their own).

Create a new hosted layer in ArcGIS Online using these settings:

- Polygon Geometry
- Set sharing level to:
	- Everyone (public)
	- Enable Delete Protection
- Approve Public Data Collection
- Enable editing
	- Enable feature update user tracking
	- Enable Add, Delete, Update editing on Attributes and Geometry
	- Editors can see all features
	- Editors can edit all features
	- Anonymous users can only add new features

Add the following fields:

| Field Name | Field Type | Field Alias |
| ---------- | ----------- | ---------- |
|adpt_pblctn_yr|Integer|Year of Plan Adoption or Publication|
|create_dt|String|Created Date|
|dept_nm|String|Name of Responsible Planning Entity|
|enty_id|String|Entity Identifier|
|enty_nm|String|Entity Name|
|mode_bcycl|Integer|Mode Bicycle|
|mode_micmobil|Integer|Mode Micromobility|
|mode_othr|Integer|Mode Other|
|mode_ped|Integer|Mode Pedestrian|
|plan_dscr|String|Plan Description|
|plan_id|String|Plan Identifier|
|plan_nm|String|Plan Name|
|cntct_nm|String|Contact Name|
|cntct_phn_nbr|String|Contact Phone Number|
|cntct_phn_ext|String|Contact Phone Number Extension|
|cntct_eml|String|Contact Email|
|cntct_dir_phn_nbr|String|Contact Direct Phone Number|
|plan_url|String|Plan URL|
|enty_type|String|Entity Type|
|plan_tags|String|Plan Tags|
|plan_gis|String|Plan GIS URL|
|plan_page_numbrs|String|Plan Page Numbers|

Current layer: https://txdot.maps.arcgis.com/home/item.html?id=00cc5bf73ee140c9a6d58521b0ab68d8

**Plan Inventory** - this hosted feature layer is the production layer used to house plans that have been validated by PTN. 

- Polygon Geometry
- This layer contins personally identifiable information
- Set sharing level to:
	- Organization
	- TTPP WA11 Transportation Plan
	- Enable Delete Protection
	- Approve Public Data Collection
	- Enable editing
	- Enable feature update user tracking
	- Enable Add, Delete, Update editing on Attributes and Geometry
	- Editors can see all features
	- Editors can edit all features

Add the same fields as the Inventory Updates feature layer.

Current layer: https://txdot.maps.arcgis.com/home/item.html?id=5a372f7cfde6426ebbfdab1325b29cb2

**Plan Inventory View** - this hosted feature layer view points towards the Plan Inventory and is used to serve data and geometries to the ATPI application.

- Polygon Geometry
- Update the definition of your hosted feature layer view to remove specific fields:
	- Contact Direct Phone Number
	- Contact Email
	- Contact Name
	- Contact Phone Number
	- Contact Phone Number Extension
	- Created Date
	- Creator
	- EditDate
	- Editor
	- created_date
	- created_user
	- last_edited_date
	- last_edited_user
	- Shape__Area
	- Shape__Length
- Set sharing level to:
	- Everyone (public)
- Enable Delete Protection
	- Editing is not enabled

The fields below from the Inventory Updates layer should be visible in the Plan Inventory:

| Field Name | Field Type | Field Alias |
| ---------- | ----------- | ---------- |
|dept_nm|String|Name of Responsible Planning Entity|
|enty_id|String|Entity Identifier|
|enty_nm|String|Entity Name|
|enty_type|String|Entity Type|
|mode_bcycl|Integer|Mode Bicycle|
|mode_micmobil|Integer|Mode Micromobility|
|mode_othr|Integer|Mode Other|
|mode_ped|Integer|Mode Pedestrian|
|adpt_pblctn_yr|Integer|Year of Plan Adoption or Publication|
|plan_dscr|String|Plan Description|
|plan_id|String|Plan Identifier|
|plan_nm|String|Plan Name|
|plan_url|String|Plan URL|
|plan_tags|String|Plan Tags|
|plan_gis|String|Plan GIS URL|
|plan_page_numbrs|String|Plan Page Numbers|

Current layer: https://txdot.maps.arcgis.com/home/item.html?id=23adb148d6064e9c9cee6d26a3481224


### ATPI Web Maps

The PlanSubmissionPortal and PlanManagementPortal both utilize Esri Web Maps to allow users to interact with the spatial data being served through the Web map. 

If needed, the Web Maps can be restored / recreated from the JSON objects in the `Deployment/AGOLItems` folder. 

The following Web Maps must be configured for the ATPI applications to function correctly:

**Plan Submission Portal Map** - this is used within the PlanSubmissionPortal.

- Feature Layers contained within the Web Map:
	- Plan Inventory
	- TxDOT Districts
	- Texas County Boundaries
	- Texas City Boundaries
	- Texas MPO Boundaries
	- This Web Map must be open to the public

https://txdot.maps.arcgis.com/home/item.html?id=d0203db46c1f49bd884c09bb9955c810

**Plan Management Portal Map** - this is used within the PlanManagementPortal.

- Feature Layers contained within the Web Map:
	- Plan Inventory
	- Inventory Updates
	- This layer is shared with the TxDOT Esri organization and with the PTN Esri group

https://txdot.maps.arcgis.com/home/item.html?id=ff5ce314118b4fcbb95d24a3ea2ab388

### ATPI Registered Applications

**Plan Management Portal Application Item** - the PlanManagementPortal needs to be registered with TxDOT's AGOL in order to leverage Ping identity.

The URL associated with the PlanManagementPortal needs to be registered within the Esri Application Item.

https://txdot.maps.arcgis.com/home/item.html?id=e2a5a69c7db34e65a4bb3aa2ee44209e

### Ping Identity Integration

The PlanManagementPortal uses TxDOT's Ping Identity to authenticate PTN users with the application. As noted above, an Esri application item needs to be configured within the Esri environment where the application is to be deployed.

Authentication is handled using Esri's out-of-the-box OAuthInfo module. The module is integrated within the PlanVerficiation widget (widgets/PlanVerification).

```javascript
	// application authentication when widget loads
	var authentication = new OAuthInfo({
	'appID' : 'kZypxLHphLTa49g8'
	});
	var authenticated = esriId.registerOAuthInfos([authentication]);
```

The code above and the Esri application item registration allows Ping identity to be leveraged.

## Deploying the ATPI Static Files

Within the project repository folder, the `Deployment` folder contains all of the folders and files required for the three ATPI applications to function. The ATPI applications are pure HTML, JavaScript, and CSS, thus they simply need to be deployed to an HTTP web server. The application can be hosted with any web server (nginx, IIS, etc.). Appropriate SSL certificates must be installed.

To deploy, copy the following folders into the appropriate directory on the web server:

- `Deployment/custom `
- `Deployment/PlanManagementPortal`
- `Deployment/PlanSubmissionPortal`
- `Deployment/Plans`


## Application URLs
Unless otherwise specified, each of the three applcations will be available using the name of the folder containing the application.
	
	Plan Submission Portal - https://<domain>/PlanSubmissionPortal/
	Plan Management Portal - https://<domain>/PlanManagementPortal/
	ATPI - https://<domain>/Plans/


	Example - https://apps.highstreet.work/Plans
	Example - https://apps.highstreet.work/PlanSubmissionPortal
	Example - https://apps.highstreet.work/PlanManagementPortal

# Application (Deployment) Folder

The applications and all supporting code, packages, and information are contained within the Deployment folder within the project repository.

## AGOLItems

This folder contains a JSON static snap shot of the Plan Inventory, Plan Inventory View, and Inventory Updates feature layers. These JSON files could be used to recreate the required feature service schemas.

- ProductionLayer.geojson - Plan Inventory
- TemporaryLayer.geojson - Inventory Updates

## custom
This folder contains documentation, JavaScript code, and CSS that are used in the PlanSubmissionPortal and the PlanManagementPortal.

The following files place critical roles in both applications:

- custom.css - custom styling and bootstrap extension
- custom.js - custom JavaScript (if something goes wrong...it's probably in here somewhere...)
- bootstrap.min.js - bootstrap JavaScript
- jquery.min.js - jQuery JavaScript module
  
## PlanSubmissionPortal

Contains all Esri Web ApplicationBuilder Developer items for the PlanSubmissionPortal.

## PlanManagementPortal

Contains all Esri Web ApplicationBuilder Developer items for the PlanManagementPortal.

## Plans

Contains all custom JavaScript, HTML, and CSS for the ATPI Application.


## Third Party Data Dependencies - TxDOT Feature Services

There are several Esri feature services maintained and hosted by TxDOT that are used within the ATPI applications:

**TxDOT Districts** - https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_Districts/FeatureServer/0

**Texas County Boundaries** - https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_County_Boundaries/FeatureServer/0

**Texas City Boundaries** - https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_City_Boundaries/FeatureServer/0

**Texas MPO Boundaries** - https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_Metropolitan_Planning_Organizations/FeatureServer/0

The above feature services are used in both a mapping and spatial analysis capacity. In the even that a URL enpoint changes, the underlying ATPI Esri Web Map and application code will need to be updated.

## Personally Identifiable Information

The AGOL-hosted data layers for the ATPI contain Personal Identifiable Information (PII) that uniquely identifies the individuals who submit plans into the inventory. Appropriate safegaurds are in place to protect the security of PII contained within the application.

Access to PII within the data layers and application is restricted to authenticated members of the appropriate user groups. All individuals with the group membership and permissions required to view the PII within the application are aware of their responsibility to protect the data to which they have access.

PII within the application:

**Inventory Updates** - this hosted feature layer is used to house temporary plan information. 

- Agency or Entity
- Contact Direct Phone Number
- Contact Email
- Contact Name
- Contact Phone Number
- Contact Phone Number Extension

**Plan Inventory** - this hosted feature layer is the production layer used to house plans that have been validated by PTN. 

- Agency or Entity
- Contact Direct Phone Number
- Contact Email
- Contact Name
- Contact Phone Number
- Contact Phone Number Extension