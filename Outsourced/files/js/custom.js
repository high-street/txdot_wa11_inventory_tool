$('.filtrbtn').click(function(){
        $('.filter').toggle()
        // $('.triangle').toggle()
})

$('.applybtn').click(function(){
    $('.filter').hide()
    // $('.triangle').hide()
})



$('.modalSlider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // adaptiveHeight: true
    arrows: true,
    prevArrow: '<button class="btn btn-prev"><img src="img/left.png" class="img-fluid" alt=""></button>',
    nextArrow: '<button class="btn btn-next"><img src="img/right.png" class="img-fluid" alt=""></button>',
    responsive: [
        
        {
          breakpoint: 991,
          settings: {
            arrows: false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
  });
