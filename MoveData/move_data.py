"""
Move data from one rest service to another.

Author:
Chapman Munn, Senior Consultant
Date: 10/6/2021
"""

import requests
import pprint
import json
from requests.api import request

from requests.models import requote_uri

from_url   = 'https://arcgis.highstreet.work/server/rest/services/Hosted/Plan_Inventory_View/FeatureServer/0'
to_url = 'https://services8.arcgis.com/wCXoXv6uVTCQuZ7q/ArcGIS/rest/services/ProductionLayer/FeatureServer/0'

features = requests.get(
	url = from_url + '/query',
	params = {
		'where' : '1=1',
		'returnGeometry' : True,
		'f' : 'pjson',
		'outFields' : '*'
	}
).json()

for item in features['features']:
	if 'geometry' in item:
		obj = {
			'attributes' : item['attributes'],
			'geometry' : item['geometry']
		}
		upload = [json.dumps(obj)]
		insert = requests.post(
			url = to_url + '/addFeatures',
			data = {
				'features' : upload,
				'f' : 'pjson'
			}
		).json()
		print (insert)