# HS Portal and TxDOT AGOL - Local Developement

Instructions for interacting with HS and TxDOT Development environments.

		cd into appropriate folder (WABHighStreet or WABTxDOT)
		cd into server folder
		High Street -> node server.js -port=3355
		TxDOT       -> node server.js -port=3344

## HS Portal

https://localhost:3355/webappbuilder/ - this url has been white listed within the HS Portal for the associated portal application item (0b246d115f1e4862b8eb17906a4a81f7)

## TxDOT AGOL

https://chapmanmunnmachine.local:3344/webappbuilder/ - this url has been white listed within TxDOT's AGOL for the associated portal application item (7b4683caac1a4580be954ae15f0732c1)

# Custom WAB Developer Steps

Apply the following custom steps locally:

## Plan Submission Portal and Plan Management Portal

### Add Custom JavaScript and CSS and required Packages

		Add the following to the bottom of the app's index.html file:

		<!-- jQuery -->
		<script type="text/javascript" src="../custom/jquery.min.js"></script>

		<!-- tags -->
		<script type="text/javascript" src="../custom/bootstrap-tagsinput.js"></script>

		<!-- typeahead -->
		<script type="text/javascript" src="../custom/typeahead.js",></script>
		
		<!-- tags -->
		<link rel = "stylesheet" href = "../custom/bootstrap-tagsinput.css">

		<!-- custom css -->
		<link rel="stylesheet" type="text/css"  href="../custom/custom.css">

## Plan Submission Portal

### DataEntryWorkflow Widget

		Within the app's config.json file, set the lockable widget width to 500:

		"panel": {
			"uri": "themes/JewelryBoxTheme/panels/LDockablePanel/Panel",
			"position": {
			"left": 0,
			"top": 40,
			"bottom": 0,
			"width": 500,
			"relativeTo": "browser"
			}
		},

## Plan Management Portal

### PlanVerification Widget

		Within the app's config.json file, set the lockable widget width to 800:

        "panel": {
          "uri": "themes/JewelryBoxTheme/panels/LDockablePanel/Panel",
          "position": {
            "left": 0,
            "top": 40,
            "bottom": 0,
            "width": 800,
            "relativeTo": "browser"
          }
        },

# Deploy to apps.highstreet.work

	ssh into the apps.highstreet.work  

## Create symlink
	cd into var/www/html
	find . -type l -ls == see existing symlinks
	rm <symlink_name> == remove the symlink
	ln -s <source_path> <new_path>

	WA11 paths:
	/home/cmunn/wa11/Deployment/Plans
	/home/cmunn/wa11/Deployment/PlanSubmissionPortal
	/home/cmunn/wa11/Deployment/PlanManagementPortal
	/home/cmunn/wa11/Deployment/custom

	Symlinks:

	ln -s /home/cmunn/wa11/Deployment/Plans /var/www/html/Plans

	ln -s /home/cmunn/wa11/Deployment/PlanSubmissionPortal /var/www/html/PlanSubmissionPortal

	ln -s /home/cmunn/wa11/Deployment/PlanManagementPortal /var/www/html/PlanManagementPortal

	ln -s /home/cmunn/wa11/Deployment/custom /var/www/html/custom


## Update project files
	cd into wa11
	git pull
		