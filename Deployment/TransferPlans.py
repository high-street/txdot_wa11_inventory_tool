"""
Transfer plans from one service to another
"""

import requests
import json

from_service = 'https://services8.arcgis.com/wCXoXv6uVTCQuZ7q/ArcGIS/rest/services/ProductionLayerView/FeatureServer/0/query'
to_service = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/ArcGIS/rest/services/Plan_Inventory/FeatureServer/0/addFeatures'

params = {
	'where' : '1=1',
	'outFields' : '*',
	'returnGeometry' : True,
	'f' : 'json'
}

features = requests.get(url = from_service, params = params).json()
if 'features' in features:
	print (len(features['features']))
	for feature in features['features']:
		print (feature['attributes'])
		data = {'features' : [json.dumps(feature)]}
		p = requests.post(url = to_service, data=data)
		print (p)