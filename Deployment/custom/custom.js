/*
Custom JavaScript code used in the TxDOT Plan Submission Portal and Plan Management Portal.

Author: Chapman Munn, Senior Consultant
Date: 02/22/2021
*/

// ------------------------- CONFIGURATION --------------------------

// constant objectid field (objectid vs. OBJECTID)
const objectid_field = 'objectid';

// Links
var temporary_layer  = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Inventory_Updates/FeatureServer/0';
var production_layer = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/ArcGIS/rest/services/Plan_Inventory/FeatureServer/0';
var production_view  = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/ArcGIS/rest/services/Plan_Inventory_View/FeatureServer/0';
var district_layer   = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_Districts/FeatureServer/0';
var county_layer     = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_County_Boundaries/FeatureServer/0';
var city_layer       = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_City_Boundaries/FeatureServer/0';
var mpo_layer        = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_Metropolitan_Planning_Organizations/FeatureServer/0'

// layer names -> used for figuring out what things are called in the viewer
var temporary_layer_name  = 'Inventory_Updates';
var production_layer_name = 'Plan_Inventory';

// set internal url links
var origin = window.location.origin;
if (origin === 'file://' || origin === 'https://chapmanmunnmachine.local:3344'){
	origin = 'https://maps.txdot.gov/';
}

// Sites
var phub = origin + '/Plans';
var psp  = origin + '/PlanSubmissionPortal';
var pmp  = origin + '/PlanManagementPortal';

// detect browser type
var isFirefox
try{
	isFirefox = typeof InstallTrigger !== 'undefined';
}
catch (error) {
	isFirefox = false;
}

class Plan{
	/*
	A Class to hold data related to a selected plan within the application
	*/
	
	assemble_object = function(){
		/*
		Assemble class data into an object that can be passed
		to a rest endpoint

		Returns:
		data -- the object containing attributes and geometry
		*/
		var attributes = {

			create_dt : this.create_dt,
			dept_nm : this.dept_nm,

			enty_type : this.enty_type,
			enty_nm : this.enty_nm,
			enty_id : (this.enty_id === undefined) ? null : this.enty_id,

			plan_nm : this.plan_nm,
			plan_id : (this.plan_id === undefined) ? null : this.plan_id,

			plan_dscr : this.plan_dscr,
			plan_page_numbrs : this.plan_page_numbrs,
			plan_url : this.plan_url,
			plan_gis : this.plan_gis,
			
			cntct_nm : this.cntct_nm,
			cntct_phn_nbr : this.cntct_phn_nbr,
			cntct_phn_ext : this.cntct_phn_ext,
			cntct_eml : this.cntct_eml,

			mode_bcycl : this.mode_bcycl,
			mode_micmobil : this.mode_micmobil,
			mode_othr : this.mode_othr,
			mode_ped : this.mode_ped,

			adpt_pblctn_yr : this.adpt_pblctn_yr,

			plan_tags : this.plan_tags
		};

		// add the production OID and value if this is an update
		if (this.new_update === 'update'){
			attributes[objectid_field] = this.production_objectid;
		}

		// assemble the enty_nm value
		if (attributes.enty_id === null){
			if (attributes.enty_nm != null){
				attributes['enty_id'] = attributes.enty_nm.split(' ').filter(function(el){return el != ''}).join('_').toLowerCase();
			}
		}

		// assemble the enty_id value
		if (attributes.plan_id === null){
			if (attributes.enty_id != null && attributes.plan_nm != null){
				attributes['plan_id'] = attributes.enty_id + '_' + attributes.plan_nm.split(' ').filter(function(el){return el != ''}).join('_').toLowerCase().replace(/[^a-z0-9+_]+/gi, '');
			}
		}

		// created date
		if (attributes.create_dt === null){
			attributes.create_dt = date;
		}

		var data = {'attributes' : attributes, 'geometry' : this.geometry};
		return data
	}

	create_geometry = function(geometry){
		/*
		Create a geometry related to what the user draws or selects
		as an existing geometry. The map extent and center will be
		set based on the created geometry.

		Inputs:
		geometry -- a geometry object
		*/
		// create graphic and add it to map
		geometry['spatialReference'] = {wkid: 4326};
		var graphic = new esri.Graphic({'geometry' : geometry});
		_viewerMap.graphics.add(graphic);
		_viewerMap.graphics.graphics[0].setSymbol(polygon_symbology)

		// get geometry centroid and extent
		var center = esri.geometry.Polygon(geometry).getCentroid();
		var extent = esri.geometry.Polygon(geometry).getExtent();

		// zoom to centroid and extent
		_viewerMap.centerAt(center);
		_viewerMap.setExtent(extent);

		// set the PlanClass geometry_object
		PlanClass.geometry = geometry;
	}

	set_class_attributes = function(attributes){
		/*
		Set the PlanClass attributes

		Inputs:
		attributes -- an object containing attribute data
		*/
		var keys = Object.keys(attributes);
		keys.forEach(item => {
			this[item] = attributes[item];
		})
	}

}

// initialize plan class
var PlanClass = new Plan;

/*
Application Objects
*/

// variables
var warning_color = '#ff8a8a';
var sucess_color  = '#c7f0d1';
var test_color    = '#ebd834';
var txdot_orange  = '#CC7B29';
var txdot_blue    = '#14375a';

var normalization_spec = {
	'create_dt' : 'Created Date',
	'enty_type' : 'Entity Type',
	
	'plan_dscr' : 'Plan Description',
	'plan_nm' : 'Plan Name',
	'plan_url' : 'Plan URL',

	'dept_nm' : 'Name of Responsible Planning Entity',
	'cntct_nm' : 'Contact Name',
	'cntct_phn_nbr' : 'Contact Phone Number',
	'cntct_phn_ext' : 'Extension',
	'cntct_eml' : 'Contact Email',

	'mode_bcycl' : 'Mode Bicycle',
	'mode_micmobil' : 'Mode Micromobility',
	'mode_othr' : 'Mode Other',
	'mode_ped' : 'Mode Pedestrian',

	'layer' : 'layer',
	'objectid' : 'objectid',
	'name' : 'Name',
	'type' : 'Type'
}

var polygon_symbology = esri.symbol.SimpleFillSymbol(
	esri.symbol.SimpleFillSymbol.STYLE_SOLID,
	new esri.symbol.SimpleLineSymbol(
		esri.symbol.SimpleLineSymbol.STYLE_SOLID, 
	new esri.Color([255,0,0,0.8]), 2), 
	new esri.Color([255,0,0,0.5])
);

var initial_extent = esri.geometry.Extent(
	{
		"xmin": -12315667.352189148,
		"ymin": 2811459.4146376792,
		"xmax": -10931239.895888405,
		"ymax": 4555446.651991797,
		"spatialReference": {
			"wkid": 102100
		},
	}
)

// Link list
var layers_object = {
	'temporary' : temporary_layer,
	'production' : production_view,
	'district' : district_layer,
	'county' : county_layer,
	'city' : city_layer,
	'mpo' : mpo_layer
}

/*
Application Functions
*/

function feature_resource_check(){
	/*
	Check to make sure that resources respond. Alert the user with a modal popup if there is an issue.
	*/

	let resources;

	if (application === 'psp'){

		resources = {
			'district_layer' : district_layer,
			'county_layer' : county_layer,
			'city_layer' : city_layer,
			'mpo_layer' : mpo_layer
		};

	}

	else {
		resources = {
			'temporary_layer' : temporary_layer,
			'production_layer' : production_view,
			'district_layer' : district_layer,
			'county_layer' : county_layer,
			'city_layer' : city_layer,
			'mpo_layer' : mpo_layer
		};
	}

	var fail = [];

	let layers = Object.keys(resources);
	layers.forEach(item => {

		var url = resources[item];

		try{

			var t0 = performance.now();
			var layer_data = query_layer_information(url = url);
			var t1 = performance.now();

			if ((t1 - t0) > 5000){
				console.log('Slow')
				fail.push(url);
			}

			if ('error' in layer_data === true){
				fail.push(url);
			}

		}
		catch (error){
			fail.push(url);
		}

	})	
	
	if (fail.length > 0){

		var alert = document.createElement('div');

		var header = document.createElement('p');
		header.innerText = `
			The following resources responded slowly, did not respond at all, or contained no features.
			Please contact TxDOT if this issue persists.
		`
		alert.appendChild(header)

		var ul = document.createElement('ul');
		alert.appendChild(ul);
		fail.forEach(item =>{
			var li = document.createElement('li');
			li.innerText = item;
			ul.appendChild(li)
		})

		dijit_basic_modal.set('content', alert);
		dijit_basic_modal.show()
	}
}

function date_string_to_year(date_string){
	/*
	Extract the full year from a date string

	Inputs:
	date_string -- a string representation of a date

	Returns:
	value -- the full year extracted from the converted date string or the string itself (if there's and issue)
	*/
	var value;
	try {
		value = new Date(date_string).getFullYear();
	} catch (error) {
		value = date_string;
	}
	return value
}

function check_cntct_eml(){
	/*
	Check the cntct_eml while it is being entered. The user
	will be alerted if the cntct_eml does not meet standard
	convention by the input being highlighted
	*/
	var email = document.getElementById('cntct_eml').value;
	if ([undefined, null, ''].includes(email) === true){
		document.getElementById('cntct_eml').style.backgroundColor = 'white';
	}
	else {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var test = re.test(email.toLowerCase());
		if (test === false){
			document.getElementById('cntct_eml').style.backgroundColor = test_color;
		}
		else {
			flash_elm_by_id(elm = 'cntct_eml', alert = 'success');
		}

	}
}

function launch_plan_url(){
	/*
	Attempt to launch and test the plan_url. Alerts
	the user in a modal if there is an issue with the entered
	plan_url. Launches the plan in a new tab if everything works
	as intended.
	*/
	var url = document.getElementById('plan_url').value;
	if ([undefined, null, ''].includes(url) === true){
		var message = 'Please enter a Plan URL.'
		dijit_basic_modal.set('content', message);
		dijit_basic_modal.show();
	}
	else{
		window.open(url = url, target = '_blank')
		console.log(window)
	}
}

function check_url(element){
	/*
	Check the plan_url while it is being entered. The
	user will be alerted if the url doesn't conform
	to normal standards by making the input section red.
	*/

	var url = document.getElementById(element).value;

	if ([undefined, null, ''].includes(url) === true){
		document.getElementById(element).style.backgroundColor = 'white';
	}

	else{
		var test = false;
		try {
			var url = new URL (url);
			test = true;
		} 
		catch (error) {
			var regex = url.match(/(www\.)?([^@])([a-z]*\.)(com|net|edu|org|gov)(\.au)?(\/\S*)?$/g);
			if (regex != undefined){
				test = true;
			}
		}
		if (test === true){
			flash_elm_by_id(elm = element, alert = 'success')
		}
		else {
			document.getElementById(element).style.backgroundColor = test_color;
		}
	}
}

function input_maxlength_test(elm, maxlength, min_value, max_value, flash = false){
	/*
	Runs a test on the input field to limit the elm value
	to whatever the max value is.

	Inputs:
	elm -- the element to test
	maxlength -- the maximum length of the input
	min_value -- the minimum allowed value
	max_value -- the maximum allowed value
	flash -- whether to flash the elm. This can be used to signify if an item has a min number of characters.

	*/
	var length = elm.value.length;
	if (length > maxlength){
		elm.value = elm.value.slice(0, maxlength)
	}
	if (length === maxlength){
		var value = parseInt(elm.value);
		if (value < min_value){
			elm.value = min_value;
		}
		if (value > max_value){
			elm.value = max_value;
		}
		if (flash === true){
			var elm_id = elm.id;
			flash_elm_by_id(elm = elm_id, alert = 'success')
		}
	}
	else {
		if (flash === true){
			elm.style.backgroundColor = test_color;
		}
	}
}

function populate_plans_dropdown(layer, populate_elm_id, populate_field){
	/*
	Populate a select element with option from a layer. The text will
	be the plan name and the value will be the plan objectid. The feature JSON
	will be stringified in a json DOM attribute.
	
	Inputs:
	layer -- the layer to pull from
	populate_elm_id -- the id of the select element to populate
	populate_field -- the name of the field to populate with
	*/
	var select = document.getElementById(populate_elm_id);
	select.innerHTML = '';

	var option = document.createElement('option');
	option.innerText = 'Select a Plan to Review';
	option.selected = true;
	option.disabled = true;
	select.appendChild(option);
	
	var entered_plans = query_layer(url = layer);

	// plans are returned from the layer
	if (entered_plans.features.length > 0){
		entered_plans.features.forEach(item => {
			console.log(item);
			var field = item.attributes[populate_field];
			var objectid = item.attributes[objectid_field];
			objectids.push(objectid);
			var option = document.createElement('option');
			option.innerText = field;
			option.value = objectid;
			option.setAttribute('json', JSON.stringify(item));
			select.appendChild(option);
		})
	}

	// no plans are returned
	else {
		document.getElementById('plan_select').disabled = true;
		document.getElementById('next').disabled = true;
		dijit_basic_modal.set('content', 'There are no plans awaiting review at this time.')
		dijit_basic_modal.show();

	}
}

function create_spinner(elm, modal = false){
	/*
	Create a spinner element and attach it to an elm

	Inputs:
	elm -- the element to attach the spinner to
	modal -- a boolean flag if the element is a dijit modal or not
	*/
	var div = document.createElement('div');

	var spinner = document.createElement('span');
	spinner.classList.add('spinner-border', 'mr-2');
	spinner.style = 'width: 2rem; height: 2rem;';

	var loading = document.createElement('span');
	loading.classList.add('h4');
	loading.innerText = 'Loading...';

	div.appendChild(spinner);
	div.appendChild(loading);

	if (modal === true){
		elm.set('content', div);
	}
	else{
		elm.innerHTML = '';
		elm.appendChild(div);
	}

}

function configure_phone_number(){
	/*
	Configure a phone number. This is called on onkeydown.
	*/
	var phone_number = document.getElementById('cntct_phn_nbr').value.toString();
	var last_item = phone_number.slice(-1);
	var reg = new RegExp("[0-9]");

	if ([undefined, null, ''].includes(phone_number) === true){
		document.getElementById('cntct_phn_nbr').style.backgroundColor = 'white';
	}
	else{
		if (reg.test(last_item) === true){

			if (phone_number.length === 10){
				new_value = phone_number.slice(0,3)+"-"+phone_number.slice(3,6)+"-"+phone_number.slice(6);
				document.getElementById('cntct_phn_nbr').value = new_value;
				flash_elm_by_id(elm = 'cntct_phn_nbr', alert = 'success')
			}
			else if (phone_number.length < 12){
				document.getElementById('cntct_phn_nbr').value = phone_number.replaceAll('-', '');
				document.getElementById('cntct_phn_nbr').style.backgroundColor = test_color;
			}
	
		}
		else{
			document.getElementById('cntct_phn_nbr').value = phone_number.slice(0, -1);
		}
	}
}

function configure_phone_extension(){
	/*
	Configure a phone extension. This is called on onkeydown.
	*/
	var extension = document.getElementById('cntct_phn_ext').value;
	var last_item = extension.slice(-1);
	var reg = new RegExp("[0-9]");
	if (reg.test(last_item) === true){
		if (extension.length > 3){
			document.getElementById('cntct_phn_ext').value = extension.slice(0, -1);
		}
	}
	else{
		document.getElementById('cntct_phn_ext').value = extension.slice(0, -1);
	}

}

function saving_button(button_element){
	/*
	Turn a button into a loading spinner

	Inputs:
	button_element -- the element to turn into a spinner
	*/
	button_element.disabled = true;
	button_element.innerHTML = 'Saving Information ';
	var span = document.createElement('span');
	span.classList.add('spinner-border', 'spinner-border-sm')
	button_element.appendChild(span)
}

function restore_button(button_element, text){
	/*
	Restores a button to normal

	Inputs:
	button_element -- the element to return to norm
	text -- the test to display on the button
	*/
	button_element.disabled = false;
	button_element.innerHTML = text;
}

function capitalize_first_letter(string) {
	/*
	Capitalizes the first letter for a string

	Inputs:
	string -- the string to capitalize

	Returns:
	result -- a capitalized version of the string
	*/
	var result = string.charAt(0).toUpperCase() + string.slice(1);
	return result
  }

function todays_date(){
	/*
	Returns todays data M/D/YYYY
	*/
	var today = new Date();
	var date = (today.getMonth()+1) + '/'+today.getDate()+ '/' + today.getFullYear();
	return date
}

function draw_polygon(){
	/*
	Allow users to draw a polygon in the map
	*/
	// reset_form_function();
	PlanClass = new Plan();
	_viewerMap.graphics.clear();
	_viewerMap.infoWindow.hide();
	drawing = true;
	dijit_planning_modal.hide();
	toolbar.activate(esri.toolbars.Draw.POLYGON);
}

function add_to_map(evt) {
	/*
	Add graphics to the map and set the geometry_object variable.

	Make the enty_nm field not read-only.

	Add geometry to PlanClass.

	Inputs:
	evt -- the output from the toolbar function
	pmp -- boolean. True if used in the pmp application
	*/

	// make the enty_nm field not true
	document.getElementById('enty_nm').readOnly = false;

	// create a graphic with the drawn geometry and add it to the map
	if (application === 'psp'){
		var graphic = new esri.Graphic(evt.geometry, polygon_symbology);
	}
	else {
		var polygon = esri.geometry.Polygon(evt.geometry);
		var graphic = new esri.Graphic(geometry = polygon, symbol = polygon_symbology);
	}
	
	console.log(graphic)
	_viewerMap.graphics.add(graphic);

	// // set the map extent
	_viewerMap.centerAt(esri.geometry.Polygon(graphic.geometry).getCentroid());
	_viewerMap.setExtent(esri.geometry.Polygon(graphic.geometry).getExtent());

	 // set the geometry variable in the PlanClass
	PlanClass.geometry = graphic.geometry;

	if (application === 'psp'){

		// deactivate the toolbar and allow for map clicks
		toolbar.deactivate(); // deactivate toolbar when finished creating geometry
		drawing = false;

		// show instructions
		document.getElementById('form').style.display = '';
		document.getElementById('instructions').style.display = 'none';

		// set entity name to blank
		document.getElementById('enty_nm').value = '';

		// set entity type to custom
		PlanClass.enty_type = 'Other';
	}
}

function delete_objectids(url, objectid, token = null){
	/*
	Delete a single or multiple objectids for a specific layer

	Inputs:
	url -- the rest service delete URL
	objectid -- a single or multiple objectids in an arry

	Returns:
	a promise
	*/
	var params = {
		objectIds : parseInt(objectid),
		f : 'pjson'
	}

	if (token != null){
		params['token'] = token;
	}

	var url = url + '/deleteFeatures';
	console.log(url);
	return $.post(
		url, 
		params
	)
}

function form_submit_test(obj, geometry){
	/*
	Test an object and geometry before it is submitted to a rest endpoing to insert or update data

	Inputs:
	obj -- the object to test
	geometry -- the geometry object to test

	Returns:
	result -- Boolean. True means pass and False means fails.
	*/
	var result = true;
	var error_lt = [];
	var pass_lt = [];
	var error_geometry = null;

	if (geometry === null){
		result = false;
		error_geometry = false;
	}

	if (obj['plan_nm'] === null){
		error_lt.push('plan_nm');
		result = false;
	}
	else {
		pass_lt.push('plan_nm');
	}

	if (obj['enty_nm'] === null){
		error_lt.push('enty_nm');
		result = false;
	}
	else {
		pass_lt.push('enty_nm');
	}

	if (obj['plan_url'] === null){
		error_lt.push('plan_url');
		result = false;
	}
	else {
		pass_lt.push('plan_url');
	}

	if (obj['cntct_eml'] === null){
		error_lt.push('cntct_eml');
		result = false;
	}
	else {
		pass_lt.push('cntct_eml');
	}

	if (obj['cntct_phn_nbr'] === null){
		error_lt.push('cntct_phn_nbr');
		result = false;
	}
	else {
		pass_lt.push('cntct_phn_nbr');
	}

	if (obj['plan_dscr'] === null){
		error_lt.push('plan_dscr');
		result = false;
	}
	else {
		pass_lt.push('plan_dscr');
	}

	var modes = [obj['mode_bcycl'], obj['mode_micmobil'], obj['mode_othr'], obj['mode_ped']];
	if (modes.filter(m => m === null).length === 4){
		result = false;
		error_lt.push('mode')
	}
	else {
		pass_lt.push('mode');
	}

	return {'result' : result, 'error_fields' : error_lt, 'pass_fields' : pass_lt, 'error_geometry' : error_geometry}
}

function parse_custom_config(){
	/*
	Parses the custom.json file

	Returns:
	result -- JSON representation of the file
	*/
	var request = new XMLHttpRequest();
	var location = '../custom/custom.json';
	request.open("GET", location, false);
	request.send(null)
	var result = JSON.parse(request.responseText);
	return result
}

function feature_layer_ids(){
	/*
	Grab the associated application feature layers ids from the map

	Returns:
	obj -- an object that contains the inventory updates and plan inventory ids
	*/
	var obj = {};
	_viewerMap.graphicsLayerIds.forEach(function(item, index){
		if (item.includes(temporary_layer_name) === true ){
			obj['inventory_updates'] = item;
		}
		else if (item.includes(production_layer_name) === true){
			obj['plan_inventory'] = item;
		}
	})
	return obj
}

function query_layer(url, query = null){
	/*
	Queries a layer and returns the features within

	Inputs:
	url -- the url of the service to ping
	query -- string representation of query to pass to the service

	Returns:
	data -- an array of objects
	false -- returned in an exception is thrown
	*/
	try{
		var params = {
			'where' : '1=1',
			'outFields' : '*',
			'returnGeometry' : true,
			'f' : 'pjson',
			'outSR' : '4326'
		}
		if (query != null){
			params.where = query;
		}
		try {
			var request = new XMLHttpRequest();
			var search_params = new URLSearchParams(params).toString();
			request.open('POST', url + '/query?' + search_params, false);
			request.send(null)
			var result = JSON.parse(request.responseText);
			console.log(result)
			return result
		} catch (error) {
			var message = `
			An error occured. If this error persists please reload the application and try again.
			`;
			dijit_basic_modal.set('content', div);
			dijit_basic_modal.show();
		}
	}
	catch {
		return false
	}
}

function query_layer_with_geometry(url, geometry){
	/*
	Queries a layer with a specific point geometry and returns the intersections features.

	Inputs:
	url -- the url of the service to ping
	geometry -- the point geometry to query with.

	Returns:
	data -- an array of objects
	false -- if an exception is thrown
	*/
	try {
		var params = {
			'where' : '1=1',
			'outFields' : '*',
			'returnGeometry' : false,
			'f' : 'pjson',
			'geometry' : JSON.stringify(geometry),
			'geometryType' : 'esriGeometryPoint',
			'spatialRel' : 'esriSpatialRelIntersects',
			'inSR' : '102100'
		}
	
		var request = new XMLHttpRequest();
		var search_params = new URLSearchParams(params).toString();
		request.open('GET', url + '/query?' + search_params, false);
		request.send(null)
		var result = JSON.parse(request.responseText);
		return result		
	} 
	
	catch (error) {
		return false
	}
}

function query_layer_information(url){
	/*
	Query the layer information page and return the JSON
	representation of the layer information.

	Inputs:
	url -- the feature layer URL
	*/
	var params = {
		'f' : 'pjson',
	}
	var request = new XMLHttpRequest();
	var search_params = new URLSearchParams(params).toString();
	request.open('GET', url + '?' + search_params, false);
	request.send(null)
	var result = JSON.parse(request.responseText);
	return result
}

function grab_form_values(){
	/*
	Grab form-control inputs from the widget form and assemble and object
	containing the element ids and values.

	Returns:
	data -- an object
	*/
	
	var data = {
		'enty_nm'          : normalize_values(document.getElementById('enty_nm').value),
		'create_dt'        : normalize_values(document.getElementById('create_dt').value),
		'cntct_nm'         : normalize_values(document.getElementById('cntct_nm').value),
		'cntct_eml'        : normalize_values(document.getElementById('cntct_eml').value),
		'cntct_phn_nbr'    : normalize_values(document.getElementById('cntct_phn_nbr').value),
		'plan_nm'          : normalize_values(document.getElementById('plan_nm').value),
		'dept_nm'          : normalize_values(document.getElementById('dept_nm').value),
		'plan_url'         : normalize_values(document.getElementById('plan_url').value),
		'plan_gis'         : normalize_values(document.getElementById('plan_gis').value),
		'plan_dscr'        : normalize_values(document.getElementById('plan_dscr').value),
		'plan_page_numbrs' : normalize_values(document.getElementById('plan_page_numbrs').value),
		'adpt_pblctn_yr'   : normalize_values(document.getElementById('adpt_pblctn_yr').value),
		'cntct_phn_ext'    : normalize_values(document.getElementById('cntct_phn_ext').value),
		'plan_tags'        : normalize_values(Array.from(document.getElementsByClassName('badge-text')).map(item => {return item.innerText}).join(',')),
		'mode_bcycl'       : null,
		'mode_micmobil'    : null,
		'mode_ped'         : null,
		'mode_othr'        : null
	};

	var raw_modes = document.getElementById('mode').selectedOptions;
	if (raw_modes.length > 0){

		var text_element = (isFirefox === true) ? 'innerText' : 'outerText';
		var modes = Array.from(raw_modes).map(f => {return f[text_element]})

		if (modes.includes('Bicycle') === true){
			data['mode_bcycl'] = 1;
		}
		if (modes.includes('Pedestrian') === true){
			data['mode_ped'] = 1;
		}
		if (modes.includes('Micromobility') === true){
			data['mode_micmobil'] = 1;
		}
		if (modes.includes('Other') === true){
			data['mode_othr'] = 1;
		}

	}

	return data
}

function normalize_values(value){
	/*
	Normalize a single value. Any undefined, empty strings, or NaN values 
	will be convered to nulls.

	Inputs:
	value -- the value to normalize

	Returns:
	norm -- a normalized value
	*/
	var norm;
	if (value === undefined){
		norm = null;
	}
	else if (value === ''){
		norm = null;
	}
	else {
		norm = value;
	}
	return norm
}

function map_click_features(geometry){
	/*
	Use a map-click geometry to get features from the
	important layers. Assemble to arrays: one containing
	features relating to non-plans (lt) and another
	related to plans (existing_plan_lt).

	These arrays are then passed to another function to
	build out the user-selection-modal-tables.

	Inputs:
	geometry -- a point geometry from a map-click

	// Link list
	var layers_object = {
	'temporary' : temporary_layer,
	'production' : production_layer,
	'district' : district_layer,
	'county' : county_layer,
	'city' : city_layer,
	'mpo' : mpo_layer
	*/
	var existing_plan_lt = [];
	var lt = [];

	// pmp only worry about temp or production layers
	if (application === 'psp'){
		delete layers_object['temporary'];
	}
	else {
		layers_object = {
			'temporary' : temporary_layer,
			'production' : production_view,
		}
	}


	// get the keys for every layer in the layer object
	keys = Object.keys(layers_object);
	keys.forEach(key => {

		results = query_layer_with_geometry(url = layers_object[key], geometry = geometry);

		console.log(results)

		// error message available in response
		if ('error' in results){
			console.log('Error Accessing: {url}'.replace('{url}', layers_object[key]));
			let error_url = layers_object[key];
			let message = `Error accessing: ${error_url}`;
			alert(message);
		}

		else if (results != false && results.features.length > 0){

			results.features.forEach(item => {

				console.log(key)
				var oid_field = layer_oid_fields[key].toString();
				console.log(oid_field)
				console.log(item['attributes'])

				var obj = {}

				obj['data'] = {
					"layer" : layers_object[key], 
					"oid" : oid_field,
					"oid_value" : item['attributes'][oid_field],
					"update" : false
				};

				if (application === 'psp'){

					if (key === 'production'){
						obj['name'] = item.attributes.plan_nm;
						obj.data['update'] = true;
						obj.data['enty_type'] = item.attributes.enty_type;
						existing_plan_lt.push(obj);
					}

					if (key === 'district'){
						obj['name'] = item.attributes.DIST_NM + ' District';
						obj.data['enty_type'] = 'district';
						lt.push(obj);
					}

					if (key === 'county'){
						obj['name'] = item.attributes.CNTY_NM + ' County';
						obj.data['enty_type'] = 'county';
						lt.push(obj);						
					}

					if (key === 'city'){
						obj['name'] = 'City of ' + item.attributes.CITY_NM;
						obj.data['enty_type'] = 'city';
						lt.push(obj);						
					}

					if (key === 'mpo'){
						obj['name'] = item.attributes.MPO_NM + ' MPO';
						obj.data['enty_type'] = 'mpo';
						lt.push(obj);
					}
				}

				if (application === 'pmp'){

					if (key === 'production'){
						obj['name'] = item.attributes.plan_nm + ' (Validated Plan)';
						obj.data['update'] = true;
						obj.data['key'] = 'production';
						obj.data['enty_type'] = item.attributes.enty_type;
						existing_plan_lt.push(obj);
					}
					if (key === 'temporary'){
						obj['name'] = item.attributes.plan_nm + ' (New or Updated Plan)';
						obj.data['update'] = true;
						obj.data['key'] = 'temporary';
						obj.data['enty_type'] = item.attributes.enty_type;
						lt.push(obj);
					}

				}

				obj['data'] = JSON.stringify(obj.data)

			})

		}

		else{
			if (results === false){
				console.log('Error Accessing: {url}'.replace('{url}', layers_object[key]));
			}
			else {
				console.log('No Features Returned: {key}'.replace('{key}', key));
			}
		}

	})

	// build out modal if we find plans or hide spinner
	if (lt.length > 0 || existing_plan_lt.length > 0){
		results_to_table(
			array_of_objects = lt, 
			array_of_existing_plans = existing_plan_lt, 
		);
	}
	else {
		dijit_basic_modal.hide();
	}

	return;
}

function results_to_table(array_of_objects, array_of_existing_plans){
	/*
	Creates and returns a table containing plan results

	Inputs:
	array_of_objects -- an array of objects containing plan data
	array_of_existing_plans -- and array of objects containing existing plan data
	keys -- an array of elements to loop through
	*/

	// existing plan table
	var existing_plan_table = create_table(array_of_existing_plans);
	if (existing_plan_table != null){
		document.getElementById('modal_existing_plans').style.display = '';
		var modal_existing_plans_table = document.getElementById('modal_existing_plans_table');
		modal_existing_plans_table.innerHTML = '';
		modal_existing_plans_table.appendChild(existing_plan_table)
	}
	else {
		document.getElementById('modal_existing_plans').style.display = 'none';
	}
	
	// planning boundary table
	var planning_boundary_table = create_table(array_of_objects);
	if (planning_boundary_table != null){
		document.getElementById('modal_new_plans').style.display = '';
		var modal_new_plans_table = document.getElementById('modal_new_plans_table');
		modal_new_plans_table.innerHTML = '';
		modal_new_plans_table.appendChild(planning_boundary_table)
	}	
	else {
		document.getElementById('modal_new_plans').style.display = 'none';
	}

	dijit_basic_modal.hide();
	dijit_planning_modal.show();

	
}

function create_table(array_of_objects){
	/*
	Create a table dom element from an array of objects

	Inputs:
	array_of_objects -- an array of objects to iterate through
	and create a table from

	Returns:
	table -- a table dom element if it exists or null
	*/

	if (array_of_objects.length > 0){

		var keys = Object.keys(array_of_objects[0]);
		keys = keys.filter(key => key != 'data');
	
		// create elements
		var table = document.createElement('table');
		table.classList.add('table', 'table-hover');
		var tbody = document.createElement('tbody');


		array_of_objects.forEach(element => {
	
			// create table row
			var row = document.createElement('tr');
	
			// add onclick event
			row.setAttribute('value', element.data);
			row.setAttribute('onclick', 'table_click_to_graphic_form(this);')
	
			// iterate through passed keys and created td elements
			keys.forEach(key => {
	
				var td = document.createElement('td');
				td.innerHTML = normalize_values(element[key]);
				td.classList.add('text-center', 'text-primary');
				row.appendChild(td);
	
			})
	
			tbody.appendChild(row);
	
		});
	
		// build out table
		table.appendChild(tbody);
	
		return table

	}

	else {
		return null
	}


}

function table_click_to_graphic_form(elm){
	/*
	When a user clicks a table row, perform some actions including:
	- query the corresponding layer with the appropriate OID field and value
	- and configure the form depending on the action.
	- set the PlanClass to reflect if this is a new project or not

	A function will be called to fill in the form after selection is made.

	The PlanClass is updated with associated data.

	Inputs:
	elm -- the element that was clicked
	*/

	// reset the PlanClass
	PlanClass = new Plan();

	// make enty_nm field read only
	document.getElementById('enty_nm').readOnly = true;

	// clear graphics and form
	_viewerMap.graphics.clear();
	document.getElementById('form').reset();

	// close planning modal
	dijit_planning_modal.hide();

	// parse out value from tr
	var value = JSON.parse(elm.getAttribute('value'));
	
	// set PlanClass oid field
	PlanClass.oid = value['oid'];

	// set PlanClass update field
	PlanClass.update = value['update'];

	// query layer
	var query = query_layer(value.layer, "{oid} = {oid_value}".replace("{oid}", value.oid).replace("{oid_value}", value.oid_value));

	// if features are available then create a graphic
	if (query != false && query.features.length > 0){
		
		// grab features and fill in the form
		var attributes = query.features[0].attributes;
		if (value.update === false){
			feature_to_form(attributes);
		}
		else {
			feature_to_form(attributes, fill = true);
		}
		
		// parse out geometry
		var geometry_object = query.features[0].geometry;

		// create the plan geometry and assign it to the PlanClass
		PlanClass.create_geometry(geometry_object);

		// hide instructions and display form
		if (application === 'psp'){
			document.getElementById('form').style.display = '';
			document.getElementById('instructions').style.display = 'none';
		}

		// set temporary and production objectids in PMP and ignore in PSP
		if (application === 'pmp'){

			// plan came from the production layer
			if (value.key === 'production'){
				document.getElementById('plan_select').value = 'default';
				i_feature = objectids[0];
				PlanClass.temporary_objectid = null;
				PlanClass.production_objectid = value.oid_value;
				PlanClass.new_update = 'update';
				PlanClass.oid = value.oid;
			}

			// plan came from the temporary layer
			if (value.key === 'temporary'){
				document.getElementById('plan_select').value = value.oid_value;
				i_feature = attributes.objectid;
				PlanClass.temporary_objectid = value.oid_value;
				PlanClass.oid = value.oid;

				// check to see if the plan exists in the production layer
				var query = "plan_id = '{plan_id}'".replace('{plan_id}', attributes.plan_id)

				// assign either 'new' or 'update' to PlanClass.new_update
				plan_in_production(query);

			}
		}		
	}
	else {
		if (query === false){
			console.log('Error querying layer');
		}
		else {
			console.log('No feature returned.')
		}
	}
}

function plan_in_production(query){
	/*
	Check to see if the plan exists in production
	*/
	var prod_features = query_layer(
		url = production_view,
		query = query
	);

	// a plan exists in production
	if (prod_features.features.length > 0){
		PlanClass.production_objectid = prod_features.features[0].attributes[objectid_field];
		PlanClass['new_update'] = 'update';
	}

	// no plan exists in production
	else{
		PlanClass.production_objectid = null
		PlanClass['new_update'] = 'new';
	}
}

function feature_to_form(attributes, fill = null){
	/*
	Fill in the form with attributes from selected feature.

	Set attributes in the PlanClass
	
	Inputs:
	attributes -- the attributes from the selected feature.
	fill -- If the parameter is null then fill in the enty_nm. If the parameter is true then we will fill in the whole form.
	layer -- the rest endpoint layer
	*/

	// reset the form
	reset_form_function();

	// fill in only the enty_nm
	if (fill === null){
		var name;
		var enty;

		if ('MPO_NM' in attributes){
			name = attributes.MPO_NM + ' Planning Organization';
			
		}
		else if ('CITY_NM' in attributes){
			name = 'City of ' + attributes.CITY_NM;
			enty = 'City';
		}
		else if ('CNTY_NM' in attributes){
			name = attributes.CNTY_NM + ' County';
			enty = 'County';
		}
		else if ('DIST_NM' in attributes){
			name = attributes.DIST_NM + ' District';
			enty = 'District';
		}
		else if ('enty_nm' in attributes){
			name = attributes.enty_nm;
			enty =  attributes.enty_type;
		}
	
		document.getElementById('enty_nm').value = name;

		// set class enty_type value
		PlanClass.enty_nm = name;
		PlanClass.enty_type = enty;

	}

	// fill in all fields based on attribute data
	else{
		document.getElementById('enty_nm').value = attributes.enty_nm;
		document.getElementById('dept_nm').value = attributes.dept_nm;

		if ('cntct_nm' in attributes){
			document.getElementById('cntct_nm').value = attributes.cntct_nm;
		}
		
		if ('cntct_eml' in attributes){
			document.getElementById('cntct_eml').value = attributes.cntct_eml;
		}
		
		if ('cntct_phn_nbr' in attributes){
			document.getElementById('cntct_phn_nbr').value = attributes.cntct_phn_nbr;
		}

		if ('cntct_phn_ext' in attributes){
			document.getElementById('cntct_phn_ext').value =  attributes.cntct_phn_ext;
		}
		
		document.getElementById('plan_nm').value = attributes.plan_nm;
		document.getElementById('plan_url').value = attributes.plan_url;
		document.getElementById('plan_gis').value = attributes.plan_gis;
		document.getElementById('plan_dscr').value = attributes.plan_dscr;
		document.getElementById('plan_page_numbrs').value = attributes.plan_page_numbrs;
		document.getElementById('adpt_pblctn_yr').value = attributes.adpt_pblctn_yr;
		document.getElementById('create_dt').value = attributes.create_dt;
		
		// plan tags
		add_tag(attributes.plan_tags);

		// fill in modes
		var modes = {
			'Bicycle' : attributes.mode_bcycl,
			'Pedestrian' : attributes.mode_ped,
			'Micromobility' : attributes.mode_micmobil,
			'Other' : attributes.mode_othr 
		};
		mode_keys = ['Bicycle', 'Pedestrian', 'Micromobility', 'Other'];
		var selection = document.getElementById('mode');
		mode_keys.forEach(k => {
			if (modes[k] != null){
				var index = mode_keys.indexOf(k);
				selection[index].selected = true;
			}
		})

		// set the PlanClass attributes
		PlanClass.set_class_attributes(attributes);
	}

	// pmp specific functionality
	if (application === 'pmp'){

		// hide the data entry form
		document.getElementById('form_buttons').hidden = false;

		// check to see if the plan exists in the production layer
		var query = "plan_id = '{plan_id}'".replace('{plan_id}', attributes.plan_id);
		plan_in_production(query);

	}

}

function reset_form_function(skip_initial_extent = true){
	/*
	Resets the form and clears graphics from the mapzs

	Inputs:
	pmp -- true if this application is the Plan Management Portal
	*/
	_viewerMap.graphics.clear();
	geometry_object = undefined;
	document.getElementById('form').reset();
	document.getElementById('create_dt').value = todays_date();
	document.getElementById('plan_tags').innerHTML = '';
	if (application === 'psp'){
		document.getElementById('form').style.display = 'none';
		document.getElementById('instructions').style.display = '';
	}
	document.getElementById('cntct_eml').style.background = 'white';
	document.getElementById('plan_nm').style.background = 'white';
	document.getElementById('plan_url').style.background = 'white';
	document.getElementById('mode').style.background = 'white';
	document.getElementById('enty_nm').style.background = 'white';
	if (skip_initial_extent === true){
		_viewerMap.setExtent(initial_extent);
	}
	document.getElementById('enty_nm').readOnly = true;
	
}

function check_production_layer(enty_nm, plan_nm){
	/*
	Checks to see if a plan identifier exists witin the production layer. Creates
	a promise that can be used when the function is called

	Inputs:
	enty_nm -- the entity name
	plan_nm -- the plan name
	*/

	var url = production_layer;
	var params = {
		'where' : "enty_nm = '{enty_nm}' and plan_nm = '{plan_nm}'".replace('{enty_nm}', enty_nm).replace('{plan_nm}', plan_nm),
		'f' : 'pjson',
		'returnGeometry' : false,
		'outFields' : '*'
	};
	return $.get(url + '/query', params)
}

function form_flash(alert){
	/*
	Flash the form briefly with a specified color

	Inputs:
	alert -- success or error
	*/
	var color;
	if (alert === 'success'){
		color = sucess_color;
	}
	else {
		color = warning_color;
	}
	document.getElementById('form').style.backgroundColor = color;
	setTimeout(function(){
		document.getElementById('form').style.backgroundColor = 'white';
	},
	500
	);
}

function flash_elm_by_id(elm, alert){
	/*
	Flash the form briefly with a specified color

	Inputs:
	elm -- element id to flash
	alert -- success or error
	*/
	var color;
	if (alert === 'success'){
		color = sucess_color;
	}
	else {
		color = warning_color;
	}
	document.getElementById(elm).style.backgroundColor = color;
	setTimeout(function(){
		document.getElementById(elm).style.backgroundColor = 'white';
	},
	500
	);
}


function create_badge(text){
	/*
	Create a badge element

	Inputs:
	text -- the text to display

	Returns:
	elm -- the badge element
	*/
	var div = document.createElement('div');
	div.classList.add('badge', 'badge-light', 'mr-1', 'mt-1');
	div.onclick = function(){
		this.remove();
	}
	var elm = document.createElement('span');
	div.appendChild(elm);
	elm.classList.add('align-middle', 'badge-text');
	elm.innerText = text;

	var exit = document.createElement('span');
	exit.classList.add('text-danger');
	div.appendChild(exit);
	exit.innerText = ' x';

	return div
}

function add_tag(string = null){
	/*
	Add the text enetered into the text box to a tag that can be saved

	Inputs:
	string -- if not null then create multiple badges from string
	*/
	var tags = document.getElementById('plan_tags');

	if (string === null){
		var entry = document.getElementById('tag_input').value;
		if (entry != ''){
			var elm = create_badge(entry);
			tags.appendChild(elm);
			
		}

	}

	else{
		var strings = string.split(',');
		strings.forEach(s => {
			var elm = create_badge(s);
			tags.appendChild(elm);

		})
	}

	document.getElementById('tag_input').value = '';

}

function create_url_with_domain(string){
	/*
	Create a url using the site domain and a string

	Inputs:
	string -- site to be added to the domain

	Returns:
	result -- url
	*/
	var domain = document.location.origin;
	var result = domain + '/' + string;
	return result
}

// computed values
var layer_oid_fields = {};
var layer_keys = Object.keys(layers_object);
layer_keys.forEach(item => {
	var information = query_layer_information(layers_object[item]);
	layer_oid_fields[item] = information.objectIdField;
});