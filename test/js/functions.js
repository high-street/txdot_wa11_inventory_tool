/*
Functions fot the Texas Active Transportation Plan Inventory Application

Author: Chapman Munn, Senior Consultant
Date: 06/29/2021
*/

function date_string_to_year(date_string){
	/*
	Extract the full year from a date string

	Inputs:
	date_string -- a string representation of a date

	Returns:
	value -- the full year extracted from the converted date string or the string itself (if there's and issue)
	*/
	var value;
	try {
		value = new Date(date_string).getFullYear();
	} catch (error) {
		value = date_string;
	}
	return value
}

function capitalize_first_letter(string) {
	/*
	Capitalizes the first letter for a string

	Inputs:
	string -- the string to capitalize

	Returns:
	result -- a capitalized version of the string
	*/
	var result = string.charAt(0).toUpperCase() + string.slice(1);
	return result
  } 

function query_layer(url, query = null){
	/*
	Queries a layer and returns the features within

	Inputs:
	url -- the url of the service to ping
	query -- string representation of query to pass to the service

	Returns:
	data -- an array of objects
	false -- returned in an exception is thrown
	*/
	try{
		var params = {
			'where' : '1=1',
			'outFields' : '*',
			'returnGeometry' : true,
			'f' : 'pjson',
			'outSR' : '4326'
		}
		if (query != null){
			params.where = query;
		}
		try {
			var request = new XMLHttpRequest();
			var search_params = new URLSearchParams(params).toString();
			request.open('GET', url + '/query?' + search_params, false);
			request.send(null)
			var result = JSON.parse(request.responseText);
			return result
		} catch (error) {
			var message = `
			An error occured. If this error persists please reload the application and try again.
			`;
			dijit_basic_modal.set('content', div);
			dijit_basic_modal.show();
		}
	}
	catch {
		return false
	}
}

function query_layer_with_geometry(url, geometry, insr = '102100'){
	/*
	Queries a layer with a specific point geometry and returns the intersections features.

	Inputs:
	url -- the url of the service to ping
	geometry -- the point geometry to query with.
	insr -- the in spatial reference value

	Returns:
	data -- an array of objects
	false -- if an exception is thrown
	*/
	try {
		var params = {
			'where' : '1=1',
			'outFields' : '*',
			'returnGeometry' : false,
			'f' : 'pjson',
			'geometry' : JSON.stringify(geometry),
			'geometryType' : 'esriGeometryPoint',
			'spatialRel' : 'esriSpatialRelIntersects',
			'inSR' : insr
		}
	
		var request = new XMLHttpRequest();
		var search_params = new URLSearchParams(params).toString();
		request.open('GET', url + '/query?' + search_params, false);
		request.send(null)
		var result = JSON.parse(request.responseText);
		return result		
	} 
	
	catch (error) {
		return false
	}
}