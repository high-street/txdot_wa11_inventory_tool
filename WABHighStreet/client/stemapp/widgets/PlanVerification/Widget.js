// UPDDATE MANIFEST.JSON WITH THE NEW WIDGET NAME

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
define([
	'dojo/_base/declare', 
	'jimu/BaseWidget',
	'jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js',
	],
	function(declare, BaseWidget) {


		return declare([BaseWidget], {

			// base class
			baseClass: 'jimu-widget-form',
			
			// startup function
			startup: function() {

		},

			// on open function
			onOpen: function(){
			},

			// on close function
			onClose: function(){
			},

		});
		
});
