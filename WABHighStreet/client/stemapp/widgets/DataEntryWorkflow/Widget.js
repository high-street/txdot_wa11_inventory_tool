///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
define([
	'dojo/_base/declare', 
	'jimu/BaseWidget',
	'jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js',
	],
	function(declare, BaseWidget) {


		return declare([BaseWidget], {

			// base class
			baseClass: 'jimu-widget-form',
			
			// startup function
			startup: function() {
				document.getElementById('warning').style.visibility = 'hidden';
		},

			// on open function
			onOpen: function(){
			},

			// on close function
			onClose: function(){
			},

		});
		
	});

	// geometry variable used to store click geom
	var geometry;

	function plan_submit(){
		/*
		Submit the plan set
		*/
	
		var obj = {};
		

		obj['contact_name'] = document.getElementById('contact_name').value;
		obj['contact_email'] = document.getElementById('contact_email').value;
		obj['plan_name'] = document.getElementById('plan_name').value;
		obj['url']  = document.getElementById('url').value;
		obj['publication_date'] = document.getElementById('publication_date').value;
		obj['note'] = document.getElementById('note').value;
		obj['district'] = document.getElementById('district').value;
		obj['county'] = document.getElementById('county').value;
		obj['city'] = document.getElementById('city').value;
		obj['planning_agency'] = document.getElementById('planning_agency').value;

		console.log(obj);

		if (obj['plan_name'] != '' && geometry != undefined){
			$.post(
				'https://arcgis.highstreet.work/server/rest/services/Hosted/TxDOT_WA_11_Temp_Layer/FeatureServer/0/addFeatures', 
					{"features" : JSON.stringify(
						[
							{
								"attributes" : obj, 
								'geometry' : geometry
							}
						]
					)
					}, 
					function(data, status){
						if (status === 'success'){
							document.getElementById('plan_submit').reset();
							alert('Plan submitted!');
						}
						else {
							alert ('We have a problem on submit!');
						}
					}
			);
		}
		else{
			alert('Please fill out the required fields');
		}
	}

	// layers
	var layer_districts = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_Districts/FeatureServer/0/query';
	var layer_counties = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_County_Boundaries/FeatureServer/0/query';
	var layer_cities = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_City_Boundaries/FeatureServer/0/query';
	var layer_planning_agency = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/Texas_Metropolitan_Planning_Organizations/FeatureServer/0/query'

	// map click
	_viewerMap.on('click', function(event){

		// clear the fields on click
		$('#district').val('');
		$('#county').val('');
		$('#city').val('');
		$('#planning_agency').val('');
		// $('#geometry_option').reset();
	
		// geometry from map click
		var click_point = event.mapPoint; //set to the point on the map where the user clicked

		// parameters for service request
		var params = {
			'where' : '1=1',
			'geometry' : JSON.stringify(click_point),
			'inSR' : 102100,
			'geometryType' : 'esriGeometryPoint',
			'spatialRel': 'esriSpatialRelIntersects',
			'f' : 'pjson',
			'returnGeometry' : true,
			'outFields' : '*'
			}

			// set district value
			$.get(layer_districts, params).then(function(result){
				var name = JSON.parse(result)['features'][0].attributes['DIST_NM'];
				$('#district').val(name);
			});

			// set county value
			$.get(layer_counties, params).then(function(result){
				var name = JSON.parse(result)['features'][0].attributes['CNTY_NM'];
				$('#county').val(name);

				geometry = JSON.parse(result)['features'][0].geometry;
				console.log(geometry)

			});

			// set city value
			$.get(layer_cities, params).then(function(result){
				var name = JSON.parse(result)['features'][0].attributes['CITY_NM'];
				if (name != ''){
					$('#city').val(name);
				}
				else {
					$('#city_option').prop('disabled', true);
				}
				
			});

			// set planning value
			$.get(layer_planning_agency, params).then(function(result){
				var name = JSON.parse(result)['features'][0].attributes['MPO_NM'];
				$('#planning_agency').val(name);
			});


	});