// UPDDATE MANIFEST.JSON WITH THE NEW WIDGET NAME

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

var i_feature;
var geometry;
var selected_objectid;

function fill_form_object(obj){
	/*
	Fills in the form with data contained in the object

	Inputs:
	obj -- an object containing attribute data e.g. data.attributes
	*/
	console.log(obj)
	document.getElementById('entity_identifier').value = obj.entity_identifier;
	document.getElementById('plan_identifier').value = obj.plan_identifier;
	document.getElementById('entity_name').value = obj.entity_name;
	document.getElementById('temporary_objectid').value = obj.objectid;
	document.getElementById('created_date').value = obj.created_date;
	document.getElementById('plan_name').value = obj.plan_name;
	document.getElementById('url').value = obj.url;

}

function check_new_update(plan_identifier){
	/*
	Checks to see if a plan identifier exists witin the production layer. Creates
	a promise that can be used when the function is called

	Inputs:
	plan_identifier -- a unique plan identifier
	*/

	var url = parse_custom_config().production_layer;
	var params = {
		'where' : "plan_identifier = '{0}'".replace('{0}', plan_identifier),
		'f' : 'pjson',
		'returnGeometry' : false,
		'outFields' : '*'
	};
	return $.get(url + '/query', params)
}

function determine_new_update(result){
	/*
	Takes the result of check_new_update and adjusts the form as needed

	Inputs:
	result -- the parsed object from check_new_update
	*/
	if (result.features.length > 0){
		document.getElementById('plan_type').value = 'plan_update';
		document.getElementById('production_objectid').value = result.features[0].attributes.objectid;
	}
	else {
		document.getElementById('plan_type').value = 'new_plan';	
	}
}


define([
	'dojo/_base/declare',
	'jimu/BaseWidget',
	'dijit/_WidgetsInTemplateMixin',
	'esri/graphic',
	'esri/tasks/FeatureSet',
	'esri/symbols/SimpleLineSymbol',
	'esri/symbols/SimpleMarkerSymbol',
	'esri/Color',
	'dojo/on',
	'dojo/_base/array',
	'dojo/_base/lang',
	'esri/toolbars/draw',
	'esri/symbols/SimpleFillSymbol',
	'dijit/form/Select',
	'dijit/form/Button',
	'dijit/Dialog',
	'jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js',
	'./custom/custom_wa11.js',
	'esri/tasks/query',
	'dojo/domReady!',
	
	],
	function(
		declare, 
		BaseWidget, 
		_WidgetsInTemplateMixin, 
		Graphic, 
        FeatureSet, 
		SimpleLineSymbol, 
		SimpleMarkerSymbol, 
		Color, 
		on, 
		array, 
		lang,
        Draw, 
		SimpleFillSymbol,
		Dialog,
		Query
	) {

		return declare([BaseWidget, _WidgetsInTemplateMixin], {

			// base class
			baseClass: 'jimu-widget-form',

			postCreate: function () {
			
				// create a use this geometry button
				var btn = document.createElement('a')
				btn.setAttribute('class', 'assess_plan');
				btn.setAttribute('data-dojo-attach-point', 'assess_plan');
				document.getElementsByClassName('actionList')[0].appendChild(btn);
				document.getElementsByClassName('assess_plan')[0].innerHTML = 'Assess Plan';
				document.getElementsByClassName('assess_plan')[0].style.color = 'green';
				document.getElementsByClassName('assess_plan')[0].style.cursor = 'pointer';

			},

			startup: function() {


				// click to validate the plan
				document.getElementsByClassName('assess_plan')[0].addEventListener('click', this.assess_plan_function);

				// click cancel button
				this.own(on(this.cancel, "click", lang.hitch(this, this.cancel_function)));

				// click next feature button
				this.own(on(this.next, "click", lang.hitch(this, this.next_function)));

				// click reject plan
				this.own(on(this.reject, "click", lang.hitch(this, this.reject_function)));

				// click validate plan
				this.own(on(this.validate, "click", lang.hitch(this, this.validate_function)));

			},

			assess_plan_function : function(){
				/*
				Allows users to assess data of a specific candidate plan. The form is filled out
				with the candidate plan data points
				*/
				
				document.getElementById('plan_form').reset();
				
				// grab feature and fill form
				var data = _viewerMap.infoWindow.getSelectedFeature();
				fill_form_object(data.attributes)

				// set plan geometry
				geometry = data.geometry;

				// check to see if the plan exists in the production layer
				check_new_update(data.attributes.plan_identifier).then(function(result){
					var result = JSON.parse(result);
					determine_new_update(result);
				})

			},

			cancel_function : function(){
				/*
				Cancels the assessment and resets the form and map
				*/
				document.getElementById('plan_form').reset();
				document.getElementById('new_update_alert').remove()
				_viewerMap.infoWindow.hide();
				i_feature = undefined;
			},

			next_function : function(){
				/*
				Iterate through features to validate
				*/

				document.getElementById('plan_form').reset();
				
				// inputs for ajax request
				var url = parse_custom_config().temporary_layer + '/query';
				var params = {
					"where" : '1=1',
					"outFields" : '*',
					"returnGeometry" : false,
					"f" : 'pjson'
				}

				// grab features from the temporary layer
				$.get(url, params).then(function(result){

					var result = JSON.parse(result);
					var objectids = result.features.map(f => {return f.attributes.objectid});

					// features exist and can be iterated through
					if (objectids.length > 0){	
						if (i_feature === undefined){
							i_feature = objectids[0];
						}
						else {
							i_feature = objectids[objectids.indexOf(i_feature) + 1];
							if (i_feature === undefined){
								i_feature = objectids[0];
							}
						}

						var layer = _viewerMap.getLayer(feature_layer_ids().inventory_updates);
						var query = new Query;
						query.where = "objectid = {0}".replace('{0}', i_feature);
						layer.selectFeatures(query).then(function(result){

							var attributes = result[0].attributes;
							fill_form_object(attributes)

							geometry = result[0].geometry;
							var center = geometry.getCentroid();
							_viewerMap.centerAndZoom(center, 10);

							// check to see if the plan exists in the production layer
							check_new_update(attributes.plan_identifier).then(function(result){
								var result = JSON.parse(result);
								determine_new_update(result);
							})

						})
					
					}

					// no features exist to iterate through
					else{
						document.getElementById('dojo_modal_content').innerHTML = 'No candidate plans exist.';
						dojo_modal.show();
					}

	
				})
				


			},

			reject_function : function(){
				/*
				User clicks the reject button. The feature will be deleted from the 
				inventory updates layer
				*/
				c = confirm('Are you sure you want to reject {0}? The plan will be removed from the Inventory Updates layer.'.replace('{0}', document.getElementById('plan_name').value));
				if (c === true){
					var objectid = document.getElementById('temporary_objectid').value;
					this.delete_function(objectid, show_message = true)
				}
			},
			
			delete_function : function(objectid, show_message = null){
				/*
				Deletes the plan from the layer

				Inputs:
				objectid -- the objectid of the feature to be deleted
				show_message -- boolean flag to alert the user to the feature being deleted
				*/

				
				var params = {
					objectIds : objectid,
					f : 'pjson'
				}
				var url = parse_custom_config().temporary_layer;
				$.post(
					url + '/deleteFeatures', 
					params
				).then(function(result){
					var result = JSON.parse(result);
					if (result.deleteResults.length > 0){
						var status = result.deleteResults[0].success;
						if (status === true){
							var plan_name = document.getElementById('plan_name').value;
							document.getElementById('plan_form').reset();
							_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh()
							_viewerMap.infoWindow.hide();
							if (show_message === true){
								document.getElementById('dojo_modal_content').innerHTML = '{0} was successfully rejected.'.replace('{0}', plan_name);
								dojo_modal.show();
							}
						}
						else{
							document.getElementById('dojo_modal_content').innerHTML = 'An error occured when trying to delete {0}. Please try again.'.replace('{0}', document.getElementById('plan_name').value)
							dojo_modal.show();
						}
					}
					else{

					}
				});

			},

			validate_function : function(){

				/*
				Validate a new plan or accept update of an existing plan
				*/
				
				var temporary_objectid = document.getElementById('temporary_objectid').value;
				var obj = {};
				obj['entity_identifier'] = document.getElementById('entity_identifier').value;
				obj['plan_identifier'] = document.getElementById('plan_identifier').value;
				obj['entity_name'] = document.getElementById('entity_name').value;
				obj['plan_name'] = document.getElementById('plan_name').value;
				obj['url'] = document.getElementById('url').value;
				obj['created_date'] = document.getElementById('created_date').value;

				console.log(obj)
 
				var production_layer = parse_custom_config().production_layer;

				// new plan added to the production layer
				if (document.getElementById('plan_type').value === 'new_plan'){
					$.post(
						production_layer + '/addFeatures', 
							{"features" : JSON.stringify(
								[
									{
										"attributes" : obj, 
										'geometry' : geometry
									}
								]
							),
							'f' : 'pjson'
							}, 
						function(result){
							var result = JSON.parse(result);
							if (result.addResults.length > 0){
								if (result.addResults[0].success === true){
									document.getElementById('dojo_modal_content').innerHTML = '{0} was successfully validated'.replace('{0}', obj['plan_name']);
									dojo_modal.show();
								}
								else {
									alert('Something bad happend');
									return null
								}
							} 
							else {
								alert('Something bad happend');
								return null
							}
					})

				}

				// update an existing plan in the production layer
				else if (document.getElementById('plan_type').value === 'plan_update'){
					obj['objectid'] = document.getElementById('production_objectid').value;
					$.post(
						production_layer + '/updateFeatures', 
							{"features" : JSON.stringify(
								[
									{
										"attributes" : obj, 
									}
								]
							),
							'f' : 'pjson'
							}, 
						function(result){
							var result = JSON.parse(result);
							if (result.updateResults.length > 0){
								if (result.updateResults[0].success === true){
									document.getElementById('dojo_modal_content').innerHTML = '{0} was successfully validated'.replace('{0}', obj['plan_name']);
									dojo_modal.show();
								}
								else {
									alert('Something bad happend');
									return null
								}
							}
							else {
								alert('An error occured')
								return null
							}
					})
				}
				this.delete_function(temporary_objectid);
				_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh()
	
			},

			// on open function
			onOpen: function(){
			},

			// on close function
			onClose: function(){
			},

		});
		
});
