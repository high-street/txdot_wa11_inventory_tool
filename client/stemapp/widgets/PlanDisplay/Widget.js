// UPDDATE MANIFEST.JSON WITH THE NEW WIDGET NAME

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
var t;
var layers; // holder variable for layer urls
var searchable_plans; // holder variable for searchable plans from prod layer
var layer_ids; // holder for automagically generated layer ids
var polygon_symbology; // holder for symbology
var geometry_object; // holder for user generated geometry

define([
	'dojo/_base/declare',
	'jimu/BaseWidget',
	'dijit/_WidgetsInTemplateMixin',
	'esri/graphic',
	'esri/tasks/FeatureSet',
	'esri/symbols/SimpleLineSymbol',
	'esri/symbols/SimpleMarkerSymbol',
	'esri/Color',
	'dojo/on',
	'dojo/_base/array',
	'dojo/_base/lang',
	'esri/toolbars/draw',
	'esri/symbols/SimpleFillSymbol',
	'dijit/form/Select',
	'dijit/form/Button',
	'dijit/Dialog',
	'jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js',
	'esri/tasks/query',
	'../custom/custom.js',
	'dojo/domReady!',
	
	],
	function(
		declare, 
		BaseWidget, 
		_WidgetsInTemplateMixin, 
		Graphic, 
        FeatureSet, 
		SimpleLineSymbol, 
		SimpleMarkerSymbol, 
		Color, 
		on, 
		array, 
		lang,
        Draw, 
		SimpleFillSymbol,
		Dialog,
		Query
	) {

		return declare([BaseWidget, _WidgetsInTemplateMixin, SimpleFillSymbol], {

			// base class
			baseClass: 'jimu-widget-form',

			postCreate: function () {
	
			},

			startup: function() {



			},

			onOpen: function(){
			},


			onClose: function(){
			},

		});
		
});

