/* jshint maxlen: 150 */
define(['dojo/_base/declare',
        'jimu/BaseWidget',
        'dijit/_WidgetsInTemplateMixin',
        'esri/graphic',
        'esri/tasks/RouteTask',
        'esri/tasks/RouteParameters',
        'esri/tasks/FeatureSet',
        'esri/symbols/SimpleLineSymbol',
        'esri/symbols/SimpleMarkerSymbol',
        'esri/Color',
        'dojo/on',
        'dojo/_base/array',
        'dojo/_base/lang',
        'esri/toolbars/draw',
        'esri/tasks/GeometryService',
        'esri/symbols/SimpleFillSymbol',
        'dijit/form/Select',
        'dijit/form/Button',
        'dojo/domReady!'
    ],
    function(declare, BaseWidget, _WidgetsInTemplateMixin, Graphic, RouteTask, RouteParameters,
        FeatureSet, SimpleLineSymbol, SimpleMarkerSymbol, Color, on, array, lang,
        Draw, GeometryService, SimpleFillSymbol
    ) {
        return declare([BaseWidget, _WidgetsInTemplateMixin], {
            baseClass: 'jimu-widget-MyWidget',
            name: 'MyWidget',
            drawToolbar: null,
            geometryService: null,
            routeTask: null,
            routeParams: null,
            routes: null,
            polygonBarrierSymbol: null,
            routeSymbols: null,
            barrierSymbol: null,
            stopSymbol: null,
            drawType: null,

            postCreate: function () {
              this.inherited(arguments);

              this.routes = [];
              this.geometryService = new GeometryService("https://utility.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
              this.routeTask = new RouteTask("https://route.arcgis.com/arcgis/rest/services/World/Route/NAServer/Route_World");
              this.routeParams = new RouteParameters();
              this.routeParams.stops = new FeatureSet();
              this.routeParams.barriers = new FeatureSet();
              this.routeParams.polygonBarriers = new FeatureSet();
              this.routeParams.outSpatialReference = {
                  "wkid": 102100
              };

              this.stopSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CROSS, 15,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 3), new Color([0,0,0]));

              this.barrierSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_X, 10,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255,0,0]), 3), new Color([255,0,0]));

              this.polygonBarrierSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255,0,0,0.8]), 2), new Color([255,0,0,0.5]));

              this.routeSymbols = {
                  "Route 1": new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 0, 255, 0.5]), 5),
                  "Route 2": new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 255, 0, 0.5]), 5),
                  "Route 3": new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 255, 0.5]), 5)
              };
            },

            startup: function() {
              this.inherited(arguments);

                this.own(on(this.addStopsBtn, "click", lang.hitch(this, this.addStops)));
                this.own(on(this.clearStopsBtn, "click", lang.hitch(this, this.clearStops)));
                this.own(on(this.addBarriersBtn, "click", lang.hitch(this, this.addBarriers)));
                this.own(on(this.clearBarriersBtn, "click", lang.hitch(this, this.clearBarriers)));
                this.own(on(this.addpolygonBarriersBtn, "click", lang.hitch(this, this.addpolygonBarriers)));
                this.own(on(this.clearpolygonBarriersBtn, "click", lang.hitch(this, this.clearpolygonBarriers)));
                this.own(on(this.solveRoutesBtn, "click", lang.hitch(this, this.solveRoute)));
                this.own(on(this.clearRoutesBtn, "click", lang.hitch(this, this.clearRoutes)));

                this.routeTask.on("solve-complete", lang.hitch(this, this.showRoute));
                this.routeTask.on("error", this.errorHandler);

                this.drawToolbar = new Draw(this.map);
                this.drawToolbar.on("draw-end", lang.hitch(this, this.addToMap));
            },

            errorHandler: function(err) {
                alert("An error occured\n" + err.message + "\n" + err.details.join("\n"));
            },

            addToMap: function(evt) {
                //this.drawToolbar.deactivate();
                switch(this.drawType){
                  case "stop":
                    this.addStop(evt);
                    break;
                  case "barrier":
                    this.addBarrier(evt);
                    break;
                  case "polybarrier":
                    this.addPolygonBarrier(evt);
                    break;
                }
            },

            addStops: function() {
                this.drawType = "stop";
                this.drawToolbar.setMarkerSymbol(this.stopSymbol);
                this.drawToolbar.activate(Draw.POINT);
            },

            clearStops: function() {
                for (var i = this.routeParams.stops.features.length - 1; i >= 0; i--) {
                    this.map.graphics.remove(this.routeParams.stops.features.splice(i, 1)[0]);
                }
            },

            addStop: function(evt) {
                this.routeParams.stops.features.push(
                    this.map.graphics.add(new Graphic(evt.geometry, this.stopSymbol))
                );
            },

            addBarriers: function() {
                this.drawType = "barrier";
                this.drawToolbar.setMarkerSymbol(this.barrierSymbol);
                this.drawToolbar.activate(Draw.POINT);
            },

            //Clears all barriers
            clearBarriers: function() {
                for (var i = this.routeParams.barriers.features.length - 1; i >= 0; i--) {
                    this.map.graphics.remove(this.routeParams.barriers.features.splice(i, 1)[0]);
                }
            },

            //Adds a barrier
            addBarrier: function(evt) {
                this.routeParams.barriers.features.push(
                    this.map.graphics.add(new Graphic(evt.geometry, this.barrierSymbol))
                );
            },

            //Clears all polybarriers
            clearpolygonBarriers: function() {
                for (var i = this.routeParams.polygonBarriers.features.length - 1; i >= 0; i--) {
                    this.map.graphics.remove(this.routeParams.polygonBarriers.features.splice(i, 1)[0]);
                }
            },

            addpolygonBarriers: function(){
              this.drawType = "polybarrier";
              this.drawToolbar.setFillSymbol(this.polygonBarrierSymbol);
              this.drawToolbar.activate(Draw.POLYGON);
            },

            // Add Polygon
            addPolygonBarrier: function(evt) {
                this.routeParams.polygonBarriers.features.push(
                    this.map.graphics.add(new Graphic(evt.geometry, this.polygonBarrierSymbol))
                );
            },

            solveRoute: function() {
                this.drawToolbar.deactivate();
                this.routeTask.solve(this.routeParams);
            },

            clearRoutes: function() {
                for (var i = this.routes.length - 1; i >= 0; i--) {
                    this.map.graphics.remove(this.routes.splice(i, 1)[0]);
                }
                this.routes = [];
            },

            showRoute: function(evt) {
                this.clearRoutes();
                array.forEach(evt.result.routeResults, lang.hitch(this, function(routeResult) {
                    this.routes.push(
                        this.map.graphics.add(
                            routeResult.route.setSymbol(this.routeSymbols[this.routeSelect.get("value")])
                        )
                    );
                }));

                var msgs = ["Server messages:"];
                array.forEach(evt.result.messages, lang.hitch(this, function(message) {
                    msgs.push(message.type + " : " + message.description);
                }));

                if (msgs.length > 1) {
                    alert(msgs.join("\n - "));
                }
            }
        });
    });
