///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
define([
	'dojo/_base/declare', 
	'jimu/BaseWidget',
	'jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js',
	],
	function(declare, BaseWidget) {


		return declare([BaseWidget], {

			// base class
			baseClass: 'jimu-widget-form',
			
			// startup function
			startup: function() {
				document.getElementById('warning').style.visibility = 'hidden';
		},

			// on open function
			onOpen: function(){
			},

			// on close function
			onClose: function(){
			},

		});
		
	});

	function plan_submit(){
		/*
		Submit the plan set
		*/
	
		var obj = {};
		obj['name'] = document.getElementById('name').value;
		obj['url']  = document.getElementById('url').value;
		obj['district'] = document.getElementById('district').value;

		var pass = true;

		for (item in obj){
			if (obj[item] === ''){
				pass = false;
			}
		}

		if (pass === false){
			document.getElementById('warning').style.visibility = 'visible';
			window.setTimeout(function(){document.getElementById('warning').style.visibility = 'hidden';}, 3000);
		}

		else{
			console.log('submit!')
		}


	}

	// layers
	var layer_districts = 'https://services.arcgis.com/KTcxiTD9dsQw4r7Z/arcgis/rest/services/TxDOT_Districts/FeatureServer/0/query';

// map click marker
	var simpleMarkerSymbol = {
		type: "simple-marker",
		style: 'x',
		color: 'red', 
		outline: {
			color: 'red',
			width: 4
		}
	}

	// map click
	_viewerMap.on('click', function(event){

		// geometry from map click
		var click_point = event.mapPoint; //set to the point on the map where the user clicked
		click_point['type'] = 'point';

		// add click graphic to map
		var pointGraphic = new esri.Graphic({
			geometry: click_point,
			symbol: {type : 'simple-marker', color : 'red', size : 8}
		});
		pointGraphic['symbol'] = simpleMarkerSymbol;
		_viewerMap.graphics.add(pointGraphic)

		// parameters for service request
		var params = {
			'where' : '1=1',
			'geometry' : JSON.stringify(click_point),
			'inSR' : 102100,
			'geometryType' : 'esriGeometryPoint',
			'spatialRel': 'esriSpatialRelIntersects',
			'f' : 'pjson',
			'returnGeometry' : false,
			'outFields' : '*'
			}

			// set district value
			$.get(layer_districts, params).then(function(result){
				var name = JSON.parse(result)['features'][0].attributes['DIST_NM'];
				var int = JSON.parse(result)['features'][0].attributes['DIST_NBR'];
				$('#district').val(name)
			});


	});