"""
Test Python code
"""

import requests
import json

def ping (service):
	"""
	Ping a service to make sure it's accessible
	"""
	params = {
		'where' : '1=1',
		'returnGeometry' : False,
		'f' : 'json',
		'outfields' : '*'
	}
	result = requests.get(url=service, params=params).json()
	print (result)

ping('https://services.arcgis.com/KTcxiTD9dsQw4r7Z/ArcGIS/rest/services/Inventory_Updates/FeatureServer/0/query')