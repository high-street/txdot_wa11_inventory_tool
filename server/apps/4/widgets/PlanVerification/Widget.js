// UPDDATE MANIFEST.JSON WITH THE NEW WIDGET NAME

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

var application = 'pmp';
var i_feature; // variable used to hold an objectid while iterating through plans
var objectids = [] // hold available objectids from the temp layer
var today = new Date(); // todays date
var date = (today.getMonth()+1) + '/' + today.getDate() + '/' + today.getFullYear();
var max_year = today.getFullYear();

define([
	'dojo/_base/declare',
	'jimu/BaseWidget',
	'dijit/_WidgetsInTemplateMixin',
	'esri/graphic',
	'esri/tasks/FeatureSet',
	'esri/symbols/SimpleLineSymbol',
	'esri/symbols/SimpleMarkerSymbol',
	'esri/Color',
	'dojo/on',
	'dojo/_base/array',
	'dojo/_base/lang',
	'esri/toolbars/draw',
	'esri/symbols/SimpleFillSymbol',
	'dijit/form/Select',
	'dijit/form/Button',
	'dijit/Dialog',
	'../custom/custom.js',
	'../custom/popper.js',
	'../custom/bootstrap.min.js',
	'../custom/bootstrap-tagsinput.js',
	'../custom/typeahead.js',
	'esri/tasks/query',
	'dojo/domReady!',
	
	],
	function(
		declare, 
		BaseWidget, 
		_WidgetsInTemplateMixin, 
		Graphic, 
        FeatureSet, 
		SimpleLineSymbol, 
		SimpleMarkerSymbol, 
		Color, 
		on, 
		array, 
		lang,
        Draw, 
		SimpleFillSymbol,
		Dialog,
		Query
	) {

		return declare([BaseWidget, _WidgetsInTemplateMixin], {

			// base class
			baseClass: 'jimu-widget-form',

			postCreate: function () {
			
			},

			startup: function() {

				// run the resource check on startup
				feature_resource_check();

				// modal updates
				dijit_planning_modal.titleBar.style.backgroundColor = txdot_orange;
				dijit_basic_modal.titleBar.style.backgroundColor = txdot_orange;

				// hide the Esri popup window on load
				_viewerMap.infoWindow.domNode.hidden = true;

				// populate the drop down plans element
				populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');

				// click next feature button
				this.own(on(this.next, "click", lang.hitch(this, this.next_function)));

				// click reject plan
				this.own(on(this.reject, "click", lang.hitch(this, this.reject_function)));

				// click validate plan
				this.own(on(this.validate, "click", lang.hitch(this, this.validate_function)));

				// go nuclear
				document.getElementById('go_nuclear').addEventListener('click', this.go_nuclear_function);

				// single map click -> display graphic information
				_viewerMap.on('click', this.map_click_function)

				// plan drop down change
				document.getElementById('plan_select').addEventListener('change', this.plan_select_function);

			},

			plan_select_function : function(){
				/*
				User selects a plan from the drop down. Load plan attributes
				and pan to plan geometry in the map.

				Set the i_feature variable.

				Set the PlanClass temporary_objectid.
				*/
				reset_form_function(skip_initial_extent = false);
				PlanClass = new Plan;

				var select = document.getElementById('plan_select');
				var objectid = select.value;

				var query = "OBJECTID = '{objectid}'".replace('{objectid}', objectid);
				plan_in_production(query);

				i_feature = parseInt(objectid);

				var selected_feature = JSON.parse(select.selectedOptions[0].getAttribute('json'))
				var attributes = selected_feature.attributes;

				feature_to_form(attributes = attributes, fill = true);
				add_to_map(evt = selected_feature);

				PlanClass.temporary_objectid = objectid;
				
			},

			map_click_function : function(evt){
				/*
				Return feature data if a user clicks on the map and they are not
				drawing a custom geometry.

				Inputs:
				evt -- the result of a map click listen
				*/

				var geometry = evt.mapPoint;

				create_spinner(dijit_basic_modal, modal = true);

				dijit_basic_modal.show().then(
					function(){
						map_click_features(geometry = geometry, psp = false);
					}
				)

			},
			go_nuclear_function : function(){
				/*
				Dump all records from the temporary and production layers
				*/
				
				c = confirm('Are you sure you want to delete all records from the application?');
				if (c === true){
					var layers = parse_custom_config();
					var temp = query_layer(layers.temporary_layer);
					if (temp.features.length > 0){
						var objectids = temp.features.map(f => {return f.attributes.objectid});
						delete_objectids(layers.temporary_layer, objectids)
						.then(function(result){
							console.log(result)
						})
					}
		
					var prod = query_layer(layers.production_layer);
					if (prod.features.length > 0){
						var objectids = prod.features.map(f => {return f.attributes.objectid});
						delete_objectids(layers.production_layer, objectids)
						.then(function(result){
							console.log(result)
						})
					}
	
					_viewerMap.graphics.refresh();
					_viewerMap.setExtent(initial_extent);
					document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = '#dafbda';
					setTimeout(function(){
						document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = 'white';
					},
					500
					);
				}
				else {
					alert('That was probably a good call...');
				}
			},

			next_function : function(){
				/*
				Iterate through features to validate
				*/

				// reset the form and the map
				reset_form_function(skip_initial_extent = false);
				_viewerMap.graphics.clear();

				// reset the PlanClass
				PlanClass = new Plan;

				// modal
				create_spinner(dijit_basic_modal, modal = true)
				dijit_basic_modal.show().then(function(){

					// grab objectids if objectids is undefined (on load)
					if (objectids.length === 0){
						var features = query_layer(url = temporary_layer, query = null)
						objectids = features.features.map(f => {return f.attributes[objectid_field]});
					}

					// features exist and can be iterated through
					if (objectids.length > 0){	
						if (i_feature === undefined){
							i_feature = objectids[0];
						}
						else {
							i_feature = objectids[objectids.indexOf(i_feature) + 1];
							if (i_feature === undefined){
								i_feature = objectids[0];
							}
						}
					}

					// query the temporary layer
					var query = "OBJECTID = ?".replace('?', i_feature);
					var feature = query_layer(url = temporary_layer, query = query);
					console.log(feature)
					if (feature.features.length > 0){

						var feature = feature.features[0];
						var attributes = feature.attributes;
						var geometry = feature.geometry;

						// set geometry object to current geometry
						geometry_object = geometry;

						// fill in the form and add graphic to map
						feature_to_form(attributes = attributes, fill=true);
						add_to_map(evt = feature);

						// set the temporary objectid value
						PlanClass.temporary_objectid = attributes[objectid_field];

						// set the select drop down
						var select = document.getElementById('plan_select');
						select.value = attributes[objectid_field]

					}
					
					dijit_basic_modal.hide();
				// end of spinner then
				});
				
			},

			reject_function : function(){
				/*
				User clicks the reject button. The feature will be deleted from the 
				inventory updates layer
				*/
				var message;
				var layer;
				var objectid;

				if (PlanClass.temporary_objectid != null){
					message = 'Are you sure you want to reject {0}? The plan will be removed from the Inventory Updates (temporary) layer.'.replace('{0}', document.getElementById('plan_nm').value);
					layer = temporary_layer;
					objectid = PlanClass.temporary_objectid;
				}
				else {
					message = 'Are you sure you want to reject {0}? The plan will be removed from the Plan Inventory (production) layer.'.replace('{0}', document.getElementById('plan_nm').value);
					layer = production_layer;
					objectid = PlanClass.production_objectid;
				}

				c = confirm(message);
				if (c === true){
					PlanClass = new Plan;
					this.delete_function(objectid = objectid, layer = layer)
				}
			},
			
			delete_function : function(objectid, layer){
				/*
				Deletes the plan from the layer

				Inputs:
				objectid -- the objectid of the feature to be deleted
				layer -- the url of the layer to remove from
				*/

				var params = {
					objectIds : objectid,
					f : 'pjson'
				}
				console.log(params)

				$.post(
					layer + '/deleteFeatures', 
					params
				).then(function(result){

					if (result.deleteResults.length > 0){
						var status = result.deleteResults[0].success;

						if (status === true){

							PlanClass = new Plan();
							reset_form_function();
							_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh()
							_viewerMap.infoWindow.hide();

							// flash the form to alert the user that things worked and zoom to initial extent
							form_flash('success');
							_viewerMap.graphics.clear();
							_viewerMap.setExtent(initial_extent);
							populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');
						}

						else{
							document.getElementById('dojo_modal_content').innerHTML = 'An error occured when trying to delete {0}. Please try again.'.replace('{0}', document.getElementById('plan_name').value)
							dojo_modal.show();
						}
					}
					else{

					}
				});

			},

			validate_function : function(){
				/*
				Validate a new plan or accept update of an existing plan
				*/

				create_spinner(dijit_basic_modal, modal = true);
				dijit_basic_modal.show()
				.then(function(){

					// grab form values
					var form_obj = grab_form_values();
					
					// entity type
					PlanClass.set_class_attributes(form_obj);
					console.log(PlanClass)
					var obj = PlanClass.assemble_object();
					
					var attributes = obj.attributes;
					var geometry = obj.geometry;

					// perform the data validation test
					var data_test = form_submit_test(attributes, geometry)
					
					// check to make sure that required form values exist
					if (data_test.result === true){
							
						// new plan added to the production layer
						if (PlanClass.new_update === 'new'){
							$.post(
								production_layer + '/addFeatures', 
									{"features" : JSON.stringify(
										[
											{
												"attributes" : attributes, 
												'geometry' : geometry
											}
										]
									),
									'f' : 'pjson'
									}, 
								function(result){
									console.log(result)
									dijit_basic_modal.hide();
									
									// var result = JSON.parse(result);
									if (result.addResults.length > 0){
										if (result.addResults[0].success === true){
											form_flash('success');
											delete_objectids(url = temporary_layer, objectid = [PlanClass.temporary_objectid]);
											_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh();
											reset_form_function();
											PlanClass = new Plan();
											populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');
										}
										else {
											form_flash('error');
										}
									} 
									else {
										form_flash('error');
									}
							})

						}

						// update an existing plan in the production layer
						else {

							$.post(
								production_layer + '/updateFeatures', 
									{"features" : JSON.stringify(
										[
											{
												"attributes" : attributes, 
											}
										]
									),
									'f' : 'pjson'
									}, 
								function(result){

									dijit_basic_modal.hide();

									// var result = JSON.parse(result);
									if (result.updateResults.length > 0){
										if (result.updateResults[0].success === true){
											delete_objectids(url = temporary_layer, objectid = [PlanClass.temporary_objectid]);
											form_flash('success');
											_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh()
											PlanClass = new Plan();
											reset_form_function();
										}
										else {
											form_flash('error');
										}
									}
									else {
										form_flash('error');
									}
							})
						}
					}
					// form test failed
					else{

						dijit_basic_modal.hide();

						document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = warning_color;
						setTimeout(function(){
							document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = 'white';
						},
						500
						);
	
						data_test.error_fields.forEach(item => {
							document.getElementById(item).style.backgroundColor = warning_color;
						})
	
						data_test.pass_fields.forEach(item => {
							document.getElementById(item).style.backgroundColor = 'white';
						})

					}

				});
			},

		});		
});
