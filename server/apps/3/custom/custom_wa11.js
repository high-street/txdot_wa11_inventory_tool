/*
Custom JavaScript code used in the TxDOT Plan Submission Portal, 
Plan Management Portal, and the Plan Hub.

Author: Chapman Munn, Senior Consultant
Date: 02/22/2021
*/

function parse_custom_config(){
	/*
	Parses the custom_config.json file

	Returns:
	result -- JSON representation of the file
	*/
	var request = new XMLHttpRequest();
	request.open("GET", "custom/custom_wa11.json", false);
	request.send(null)
	var result = JSON.parse(request.responseText);
	return result
}

function query_layer(url){
	/*
	Queries the temporary layer and returns the features within

	Inputs:
	url -- the url of the service to ping

	Returns:
	data -- an array of objects
	*/
	var params = {
		'where' : '1=1',
		'outFields' : '*',
		'returnGeometry' : true,
		'f' : 'pjson'
	}
	var request = new XMLHttpRequest();
	var search_params = new URLSearchParams(params).toString();
	request.open('GET', url + '/query?' + search_params, false);
	request.send(null)
	var result = JSON.parse(request.responseText);
	return result
}

function add_plan_options(){
	/*

	*/
	var features = query_layer(parse_custom_config().temporary_layer);
	if (features.features.length > 0){
		var selector = document.getElementById('existing_plans')
		features.features.forEach(function(item, index){
			var option = document.createElement('option');
			if (item.attributes.plan_name != null){
				option.text = item.attributes.plan_name;
				option.value = JSON.stringify(item);
				selector.add(option);
			}
		});
	}
}