/*
Developed by High Street for the Texas Department of Transportation

Author: Chapman Munn, Senior Consultant
Date: 02/24/2021
*/

var application = 'psp';
var toolbar; // variable to store toolbar so it can be accessed by custom.js
var drawing = false; // variable to determine if the user is drawing or not
var today = new Date(); // todays date
var date = (today.getMonth()+1) + '/' + today.getDate() + '/' + today.getFullYear();
var max_year = today.getFullYear();

define(['dojo/_base/declare',
        'jimu/BaseWidget',
        'dijit/_WidgetsInTemplateMixin',
        'esri/graphic',
        'esri/tasks/FeatureSet',
        'esri/symbols/SimpleLineSymbol',
        'esri/symbols/SimpleMarkerSymbol',
        'esri/Color',
        'dojo/on',
        'dojo/_base/array',
        'dojo/_base/lang',
        'esri/toolbars/draw',
        'esri/symbols/SimpleFillSymbol',
        'dijit/form/Select',
        'dijit/form/Button',
		'dijit/Dialog',
		'../custom/custom.js',
		'../custom/popper.js',
		'../custom/bootstrap.min.js',
        'dojo/domReady!',
    ],

    function(
		declare, 
		BaseWidget, 
		_WidgetsInTemplateMixin, 
		Graphic, 
        FeatureSet, 
		SimpleLineSymbol, 
		SimpleMarkerSymbol, 
		Color, 
		on, 
		array, 
		lang,
        Draw, 
		SimpleFillSymbol,
		Dialog
    ) {

        return declare(
			[
				BaseWidget, 
				_WidgetsInTemplateMixin
			], {
            
			baseClass: 'jimu-widget-DataEntryWorkflow',
			name: 'DataEntryWorkflow',  
			drawToolbar: null,
			geometry: {},
            polygon_symbology: null,
			line_symbology: null,
            draw_type: null,

            postCreate: function () {

				this.inherited(arguments);

            },

            startup: function() {

				this.inherited(arguments);

				// run the resource check on startup
				feature_resource_check();

				// hide the form on load
				document.getElementById('form').style.display = 'none';

				// hide the Esri popup window on load
				_viewerMap.infoWindow.domNode.hidden = true;

				// set modal cursor to pointer
				document.getElementsByClassName('dijitDialogPaneContent')[0].style.cursor = 'pointer';
				
				// submit plan
				this.own(on(this.submit_plan, 'click', lang.hitch(this, this.submit_plan_to_layer)));

				// configure toolbar w/ Draw
				toolbar = esri.toolbars.Draw(_viewerMap);

				// add the geometry to the map
				toolbar.on("draw-end", add_to_map);

				// single map click -> display graphic information
				_viewerMap.on('click', this.map_click_function)

				// enter the created date
				document.getElementById('create_dt').value = date;

				// modal updates
				dijit_planning_modal.titleBar.style.backgroundColor = txdot_orange;
				dijit_basic_modal.titleBar.style.backgroundColor = txdot_orange;

            },

			map_click_function : function(evt){
				/*
				Return feature data if a user clicks on the map and they are not
				drawing a custom geometry.

				Inputs:
				evt -- the result of a map click listen
				*/
				if (drawing != true){
					var geometry = evt.mapPoint;
					
					create_spinner(dijit_basic_modal, modal = true);

					dijit_basic_modal.show().then(
						function(){
							map_click_features(geometry = geometry)
						}
					)
				}
				
			},

			submit_plan_to_layer: function(){
				/*
				Submit the plan to the temporary feature layer. Data will be grabbed
				from the form and initialized in the PlanClass. A geometry and attribute
				check will be performed on submission.

				User will be alerted to an issue with a form-flash. Required fields
				that fail the test will be highlighted in red.
				*/
				
				// submit button
				var submit_button = document.getElementById('submit_plan')

				// grab data from the form fields
				var obj = grab_form_values();

				// update PlanClass attributes
				PlanClass.set_class_attributes(obj);

				// get attribute and geometry data from the PlanClass
				var obj = PlanClass.assemble_object();
				var attributes = obj.attributes;
				var geometry = obj.geometry;
				
				// // data test
				var data_test = form_submit_test(attributes, geometry);
				
				// submit data if data test passes
				if (data_test['result'] === true){

					create_spinner(dijit_basic_modal, modal = true)
					dijit_basic_modal.show();
						
					// flash success
					form_flash(alert = 'success');

					// json object to submit
					var json_object = {
							"features" : 
								JSON.stringify([
									{
										"attributes" : attributes, 
										'geometry' : geometry
									}
								])
					,
					'f' : 'json'
					};

					try {
						$.post(
							temporary_layer + '/addFeatures', 
								json_object
								, 
								function(data, status){
	
									console.log(data)
	
									// error in response
									if ('error' in data){
	
										// restore button to normal
										restore_button(submit_button, 'Submit Plan');
	
										// flash form on error
										setTimeout(function(){
											var message = "An error occured. Please try again. If this error persists, please contact TxDOT staff."
											dijit_basic_modal.set('content', message);
											dijit_basic_modal.show();
										},
										3000
										);
	
									}
	
									// data saved successfully
									if (data.addResults[0].success === true){
	
										// rest the form
										reset_form_function();
	
										// modal success message
										var message = document.createElement('div');
										var text = document.createElement('p');
										text.innerText = `
										Thank you for your submission. Your plan has been submitted for review. 
										Most plans are published within 5 business days. 
										`
										message.appendChild(text);
										var link = document.createElement('a');
	
										link.innerText = 'Visit the Plan Hub to see approved plans';
										link.setAttribute('href', phub);
										message.appendChild(link);
	
										dijit_basic_modal.set('content', message);
										dijit_basic_modal.show();
	
									}
									
									// error when uploading data
									else {
	
										// restore button to normal
										restore_button(submit_button, 'Submit Plan');
	
										// flash form on error
										setTimeout(function(){
											var message = "An error occured. Please try again. If this error persists, please contact TxDOT staff."
											dijit_basic_modal.set('content', message);
											dijit_basic_modal.show();
										},
										3000
										);
									}
								}
						);
					} 

					catch (error) {
						// restore button to normal
						restore_button(submit_button, 'Submit Plan');

						// flash form on error
						setTimeout(function(){
							var message = "An error occured. Please try again. If this error persists, please contact TxDOT staff."
							dijit_basic_modal.set('content', message);
							dijit_basic_modal.show();
						},
						3000
						);
					}

					// restore button to normal
					restore_button(submit_button, 'Submit Plan');

				}

				// data test fails
				else {

					form_flash('warning');

					data_test.error_fields.forEach(item => {
						document.getElementById(item).style.backgroundColor = warning_color;
					})

					data_test.pass_fields.forEach(item => {
						document.getElementById(item).style.backgroundColor = 'white';
					})

					// restore button to normal
					restore_button(submit_button, 'Submit Plan');

				}
 

			},

			modal_function : function(message){
				/*
				*/
				document.getElementById('dojo_modal_content').innerHTML = message;
				dojo_modal.show();
			}

		// end of return 
        });

});
