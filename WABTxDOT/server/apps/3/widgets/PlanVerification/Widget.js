// UPDDATE MANIFEST.JSON WITH THE NEW WIDGET NAME

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

var application = 'pmp';
var i_feature; // variable used to hold an objectid while iterating through plans
var objectids = [] // hold available objectids from the temp layer
var today = new Date(); // todays date
var date = (today.getMonth()+1) + '/' + today.getDate() + '/' + today.getFullYear();
var max_year = today.getFullYear();

define([
	'dojo/_base/declare',
	'jimu/BaseWidget',
	'dijit/_WidgetsInTemplateMixin',
	'esri/arcgis/OAuthInfo',
	'esri/IdentityManager',
	// 'esri/graphic',
	// 'esri/tasks/FeatureSet',
	// 'esri/symbols/SimpleLineSymbol',
	// 'esri/symbols/SimpleMarkerSymbol',
	// 'esri/Color',
	'dojo/on',
	"esri/layers/FeatureLayer",
	// 'dojo/_base/array',
	'dojo/_base/lang',
	// 'esri/toolbars/draw',
	// 'esri/symbols/SimpleFillSymbol',
	'dijit/form/Select',
	'dijit/form/Button',
	// 'dijit/Dialog',
	// 'esri/tasks/query',
	'../custom/custom.js',
	'../custom/popper.js',
	'../custom/bootstrap.min.js',
	'../custom/bootstrap-tagsinput.js',
	'../custom/typeahead.js',
	'dojo/domReady!',
	
	],
	function(
		declare, 
		BaseWidget, 
		_WidgetsInTemplateMixin, 
		OAuthInfo,
		// IdentityManager,
		esriId,
		// Graphic, 
        // FeatureSet, 
		// SimpleLineSymbol, 
		// SimpleMarkerSymbol, 
		// Color, 
		on, 
		FeatureLayer,
		// array, 
		lang,
        // Draw, 
		// SimpleFillSymbol,
		// Dialog,
		// Query,
	) {

		return declare([BaseWidget, _WidgetsInTemplateMixin], {

			// base class
			baseClass: 'jimu-widget-form',

			postCreate: function () {
			
			},

			startup: function() {

				// application authentication when widget loads
				var authentication = new OAuthInfo({
					'appID' : 'kZypxLHphLTa49g8',
					'popup' : true,
				});
				var authenticated = esriId.registerOAuthInfos([authentication]);

				$(document).ready(function() {
					$.active = false;
					$('body').bind('click keypress', function() { $.active = true; });
					checkActivity(900000, 60000, 0); // timeout = 15 minutes, interval = 1 minute.
				});
				function checkActivity(timeout, interval, elapsed) {
					if ($.active) {
						elapsed = 0;
						$.active = false;
					}
					if (elapsed < timeout) {
						elapsed += interval;
						setTimeout(function() {
							checkActivity(timeout, interval, elapsed);
						}, interval);
					} else {
						esriId.destroyCredentials();
						window.location = 'https://www.txdot.gov/inside-txdot/modes-of-travel/bicycle/plan-inventory-tool.html'; // Redirect to "session expired" page.
					}
				}

				// set phub link
				document.getElementById('phub_link').setAttribute('href', create_url_with_domain('Plans'));

				// run the resource check on startup
				feature_resource_check();

				// modal updates
				dijit_planning_modal.titleBar.style.backgroundColor = txdot_orange;
				dijit_basic_modal.titleBar.style.backgroundColor = txdot_orange;

				// hide the Esri popup window on load
				_viewerMap.infoWindow.domNode.hidden = true;

				// populate the drop down plans element
				populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');

				// click next feature button
				this.own(on(this.next, "click", lang.hitch(this, this.next_function)));

				// click reject plan
				this.own(on(this.reject, "click", lang.hitch(this, this.reject_function)));

				// click validate plan
				this.own(on(this.validate, "click", lang.hitch(this, this.validate_function)));

				// click about widget link
				// this.own(on(this.about_widget, "click", lang.hitch(this, this.about_widget_function)))

				// single map click -> display graphic information
				_viewerMap.on('click', this.map_click_function)

				// plan drop down change
				document.getElementById('plan_select').addEventListener('change', this.plan_select_function);

			},

			// about_widget_function : function(){
			// 	console.log('About')
			// 	let about = dojo.byId('widgets_About_Widget_37_panel');
			// 	console.log(about)
			// },

			plan_select_function : function(){
				/*
				User selects a plan from the drop down. Load plan attributes
				and pan to plan geometry in the map.

				Set the i_feature variable.

				Set the PlanClass temporary_objectid.
				*/
				reset_form_function(skip_initial_extent = false);
				PlanClass = new Plan;

				var select = document.getElementById('plan_select');
				var objectid = select.value;

				i_feature = parseInt(objectid);

				var selected_feature = JSON.parse(select.selectedOptions[0].getAttribute('json'))
				var attributes = selected_feature.attributes;

				feature_to_form(attributes = attributes, fill = true);
				add_to_map(evt = selected_feature);

				PlanClass.temporary_objectid = objectid;
				
			},

			map_click_function : function(evt){
				/*
				Return feature data if a user clicks on the map and they are not
				drawing a custom geometry.

				Inputs:
				evt -- the result of a map click listen
				*/

				var geometry = evt.mapPoint;

				create_spinner(dijit_basic_modal, modal = true);

				dijit_basic_modal.show().then(
					function(){
						map_click_features(geometry = geometry, psp = false);
					}
				)

			},

			next_function : function(){
				/*
				Iterate through features to validate
				*/

				// reset the form and the map
				reset_form_function(skip_initial_extent = false);
				_viewerMap.graphics.clear();

				// reset the PlanClass
				PlanClass = new Plan;

				// modal
				create_spinner(dijit_basic_modal, modal = true)
				dijit_basic_modal.show().then(function(){

					// grab objectids if objectids is undefined (on load)
					if (objectids.length === 0){
						var features = query_layer(url = temporary_layer, query = null)
						objectids = features.features.map(f => {return f.attributes[objectid_field]});
					}

					// features exist and can be iterated through
					if (objectids.length > 0){	
						if (i_feature === undefined){
							i_feature = objectids[0];
						}
						else {
							i_feature = objectids[objectids.indexOf(i_feature) + 1];
							if (i_feature === undefined){
								i_feature = objectids[0];
							}
						}
					}

					// query the temporary layer
					var query = "{objectid_field} = ?".replace('?', i_feature).replace('{objectid_field}', objectid_field);
					var feature = query_layer(url = temporary_layer, query = query);
					console.log(feature)
					if (feature.features.length > 0){

						var feature = feature.features[0];
						var attributes = feature.attributes;
						var geometry = feature.geometry;

						// set geometry object to current geometry
						geometry_object = geometry;

						// fill in the form and add graphic to map
						feature_to_form(attributes = attributes, fill=true);
						add_to_map(evt = feature);

						// set the temporary objectid value
						PlanClass.temporary_objectid = attributes[objectid_field];

						// set the select drop down
						var select = document.getElementById('plan_select');
						select.value = attributes[objectid_field]

					}
					
					dijit_basic_modal.hide();
				// end of spinner then
				});
				
			},

			reject_function : function(){
				/*
				User clicks the reject button. The feature will be deleted from the 
				inventory updates layer
				*/
				var message;
				var layer;
				var objectid;

				if (PlanClass.new_update === 'new'){
					message = 'Are you sure you want to reject {0}? The plan will be removed from the Inventory Updates (temporary) layer.'.replace('{0}', document.getElementById('plan_nm').value);
					layer = temporary_layer;
					objectid = PlanClass.temporary_objectid;
				}
				else {
					message = 'Are you sure you want to reject {0}? The plan will be removed from the Plan Inventory (production) layer.'.replace('{0}', document.getElementById('plan_nm').value);
					layer = production_layer;
					objectid = PlanClass.production_objectid;
				}

				c = confirm(message);
				if (c === true){
					PlanClass = new Plan;
					this.delete_function(objectid = objectid, layer = layer)
				}
			},
			
			delete_function : function(objectid, layer){
				/*
				Deletes the plan from the layer

				Inputs:
				objectid -- the objectid of the feature to be deleted
				layer -- the url of the layer to remove from
				*/

				// user token
				var token = esriId.credentials.map(function(item){if(item.scope === 'server'){return item.token}}).filter(x => x !== undefined)[0];
				
				// parameters to pass to service
				var params = {
					objectIds : objectid,
					f : 'pjson',
					token : token
				}
				console.log(params)

				$.post(
					layer + '/deleteFeatures', 
					params
				).then(function(result){

					// an error occured
					if ('error' in result){
						form_flash('error');
					}
					else {
						if (result.deleteResults.length > 0){
							var status = result.deleteResults[0].success;
	
							if (status === true){
	
								PlanClass = new Plan();
								reset_form_function();
								_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh()
								_viewerMap.infoWindow.hide();
	
								// flash the form to alert the user that things worked and zoom to initial extent
								form_flash('success');
								_viewerMap.graphics.clear();
								_viewerMap.setExtent(initial_extent);
								populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');
	
								// hide the data entry form
								document.getElementById('form_buttons').hidden = true;
							}
	
							else{
								document.getElementById('dojo_modal_content').innerHTML = 'An error occured when trying to delete {0}. Please try again.'.replace('{0}', document.getElementById('plan_name').value)
								dojo_modal.show();
							}
						}
					}

				});

			},

			validate_function : function(){
				/*
				Validate a new plan or accept update of an existing plan
				*/

				create_spinner(dijit_basic_modal, modal = true);
				dijit_basic_modal.show()
				.then(function(){

					// grab form values
					var form_obj = grab_form_values();
					
					// entity type
					PlanClass.set_class_attributes(form_obj);
					console.log(PlanClass)
					var obj = PlanClass.assemble_object();
					
					var attributes = obj.attributes;
					var geometry = obj.geometry;

					// perform the data validation test
					var data_test = form_submit_test(attributes, geometry);

					// ser flag to false
					var success = false;

					// user token
					var token = esriId.credentials.map(function(item){if(item.scope === 'server'){return item.token}}).filter(x => x !== undefined)[0];
					
					// check to make sure that required form values exist
					if (data_test.result === true){

						var success = function(){
							/*
							Inline function to be called in the event that everything works as designed
							*/
							form_flash('success');
							var del = delete_objectids(url = temporary_layer, objectid = [PlanClass.temporary_objectid], token = token);
							
							del.then(function(result){
								console.log(result)
							})

							_viewerMap.getLayer(feature_layer_ids().inventory_updates).refresh();
							reset_form_function();
							PlanClass = new Plan();
							populate_plans_dropdown(layer = temporary_layer, populate_elm_id = 'plan_select', populate_field = 'plan_nm');
						
							// hide the data entry form
							document.getElementById('form_buttons').hidden = true;
						}
							
						// new plan added to the production layer
						if (PlanClass.new_update === 'new'){
							$.post(
								production_layer + '/addFeatures', 
									{"features" : JSON.stringify(
										[
											{
												"attributes" : attributes, 
												'geometry' : geometry
											}
										]
									),
									'f' : 'pjson',
									'token' : token
									}, 
								function(result){
									console.log(result)
									dijit_basic_modal.hide();
									
									// error occured
									if ('error' in result){
										form_flash('error');
										alert('An error occured when trying to add this plan to the Plan Inventory. Please try again.')
									}

									// no error occured
									else {
										if (result.addResults.length > 0){
											if (result.addResults[0].success === true){
												success();
											}
											else {
												form_flash('error');
												alert('An error occured when trying to add this plan to the Plan Inventory. Please try again.')
											}
										}
									}
								
							})

						}

						// update an existing plan in the production layer
						else {
							console.log('UPDATE');

							$.post(
								production_layer + '/updateFeatures', 
									{"features" : JSON.stringify(
										[
											{
												"attributes" : attributes, 
											}
										]
									),
									'f' : 'pjson',
									'token' : token
									}, 
								function(result){

									dijit_basic_modal.hide();

									// there as an issue
									if ('error' in result){
										form_flash('error');
										alert('An error occured when trying to update this plan in the Plan Inventory. Please try again.')
									}

									// no error occured
									else {
										if (result.updateResults.length > 0){
											
											if (result.updateResults[0].success === true){
												success();
											}
											else {
												form_flash('error');
												alert('An error occured when trying to update this plan in the Plan Inventory. Please try again.')
											}
										}

									}

							})
						}

					}
					// form test failed
					else{

						var message = "Please fill out or select a value for all required fields - required fields contain '(required)' in the field label.";
						dijit_basic_modal.set('content', message);
						dijit_basic_modal.show();

						document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = warning_color;
						setTimeout(function(){
							document.getElementsByClassName('jimu-widget-frame jimu-container')[0].style.backgroundColor = 'white';
						},
						500
						);
	
						data_test.error_fields.forEach(item => {
							document.getElementById(item).style.backgroundColor = warning_color;
						})
	
						data_test.pass_fields.forEach(item => {
							document.getElementById(item).style.backgroundColor = 'white';
						})

					}

				});
			},

		});		
});
